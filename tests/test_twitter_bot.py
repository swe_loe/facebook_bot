import os
from facebookbot.bot.twitter_bot import TwitterBot

# valid credentials for : @horatiusgernot / test-swe-26
# just for testing in this file!
access_token_key = os.environ['ACCESS_TOKEN_KEY_EMMA']
access_token_secret = os.environ['ACCESS_TOKEN_SECRET_EMMA']
consumer_key = os.environ['CONSUMER_KEY_EMMA']
consumer_secret = os.environ['CONSUMER_SECRET_EMMA']
bot_id = 0

f = open('tokens', 'r')
lines = f.readlines()
access_token_key = lines[0]
access_token_secret = lines[1]
consumer_key = lines[2]
consumer_secret = lines[3]
f.close()

bot = TwitterBot(access_token_key, access_token_secret, consumer_key,
                 consumer_secret, bot_id)

user_screen_name = "Williams23Emma"
user = bot.api_obj.get_user(user_screen_name)

tweet_text = 'Meiner Meinung nach sollten an allen Schulen Kekse verteilt \
werden. #cookiesfortheworld'


def test_check_rate_limit():
    # rate_limit = bot.get_rate_limit_status()
    # regex = re.compile(['a'])
    # regex.match('abc')
    # return regex
    pass


def test_post_tweet():
    bot.tweet(tweet_text)
    last_self_tweet = bot.get_last_tweet()
    assert tweet_text == last_self_tweet.text


def test_delete_last_tweet():
    last_self_tweet = bot.get_last_tweet()
    second_last_self_tweet = bot.get_last_tweet(index=1)

    bot.api_obj.destroy_status(last_self_tweet.id)
    new_last_self_tweet = bot.get_last_tweet()

    assert new_last_self_tweet == second_last_self_tweet


def test_follow():
    friendship = bot.api_obj.show_friendship(
        target_screen_name=user_screen_name)

    if not friendship[0].following:
        bot.api_obj.create_friendship(user.id)
        friendship = bot.api_obj.show_friendship(
            target_screen_name=user_screen_name)
        assert friendship[0].following

        bot.api_obj.destroy_friendship(user.id)
        friendship = bot.api_obj.show_friendship(
            target_screen_name=user_screen_name)
        assert not friendship[0].following

    if friendship[0].following:
        bot.api_obj.destroy_friendship(user.id)
        friendship = bot.api_obj.show_friendship(
            target_screen_name=user_screen_name)
        assert not friendship[0].following

        bot.api_obj.create_friendship(user.id)
        friendship = bot.api_obj.show_friendship(
            target_screen_name=user_screen_name)
        assert friendship[0].following


def test_retweet():
    last_user_tweet = bot.get_last_tweet(user)
    last_bot_tweet = bot.get_last_tweet()

    assert last_bot_tweet.text != "RT @" + user_screen_name + ": " \
        + last_user_tweet.text

    bot.retweet(last_user_tweet)
    last_bot_tweet = bot.get_last_tweet()
    print(last_bot_tweet.text)
    print(last_user_tweet.text)
    assert last_bot_tweet.text == "RT @" + user_screen_name + ": " \
        + last_user_tweet.text


def test_delete_last_tweet2():
    last_self_tweet = bot.get_last_tweet()
    second_last_self_tweet = bot.get_last_tweet(index=1)
    print(last_self_tweet.text)
    print(second_last_self_tweet.text)

    bot.api_obj.destroy_status(last_self_tweet.id)
    new_last_self_tweet = bot.get_last_tweet()

    assert new_last_self_tweet == second_last_self_tweet


def test_favorite_tweet():
    last_user_tweet = bot.get_last_tweet(user)

    if last_user_tweet.favorited:
        bot.api_obj.destroy_favorite(last_user_tweet.id)
        last_user_tweet = bot.get_last_tweet(user)
        assert not last_user_tweet.favorited

        bot.api_obj.create_favorite(last_user_tweet.id)
        last_user_tweet = bot.get_last_tweet(user)
        assert last_user_tweet.favorited

    else:
        bot.api_obj.create_favorite(last_user_tweet.id)
        last_user_tweet = bot.get_last_tweet(user)
        assert last_user_tweet.favorited

        bot.api_obj.destroy_favorite(last_user_tweet.id)
        last_user_tweet = bot.get_last_tweet(user)
        assert not last_user_tweet.favorited


def test_send_direct_message():
    bot.send_message(bot.get_user("horatiusgernot").id, "testtest")
    messages = bot.get_messages()
    assert messages[0].text == "testtest"


def test_delete_direct_message():
    messages = bot.get_messages()
    bot.api_obj.destroy_direct_message(messages[0].id)
    messages = bot.get_messages()
    assert messages[0].text != "testtest"


# def test_send_message():
#     sample_message = "Huhu Emma"
#     message_obj = bot.api_obj.send_direct_message(user.id, sample_message)
