from pytest import raises
from facebookbot.utils.media_downloader import download_media
from os.path import exists, isdir, join
from pathlib import Path
from shutil import rmtree
from requests import HTTPError


def test_download_image():
    """
    checks if directories are created in the specified structure
    and if file is downloaded
    """
    image_url = 'https://pbs.twimg.com/media/DcxahYtXkAg3GMC.jpg'
    bot_name = 'testBot'
    media_type = 'photo'
    download_media(image_url, media_type, bot_name)
    home = Path.home()
    parent_dir = 'facebookbot_data'
    media_dir = join(home, parent_dir, bot_name, media_type)
    assert isdir(media_dir)
    assert exists(media_dir)
    assert exists(join(media_dir, 'DcxahYtXkAg3GMC.jpg'))
    rmtree(join(home, parent_dir))


def test_download_ressource_unavailable():
    """
    An HTTPError should be raised if the requested ressource is
    not available e.g. response.status_code != 200
    """
    with raises(HTTPError):
        image_url = 'https://pbs.twimg.com/media/DcxahYtXkAg3GMC'
        bot_name = 'testBot'
        media_type = 'photo'
        download_media(image_url, media_type, bot_name)
