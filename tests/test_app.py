import os
from pprint import pprint
import pytest
import requests
import json
from threading import Thread
from cheroot.wsgi import Server as WSGIServer
from facebookbot.config import UnitTestConfig
from facebookbot.models import TwitterBotModel
from facebookbot.utils.db_helpers import delete_rule_from_db
from facebookbot.utils.tokens import get_tokens

base_url = 'http://localhost:5000'


@pytest.fixture(scope='session')
def http_server():
    """
    this function is run once at test start
    """
    print(UnitTestConfig.DATABASE)
    from facebookbot import create_app
    app = create_app(UnitTestConfig)
    server = ServerThread(bind_address=('127.0.0.1', 5000), app=app)
    yield server.start()
    server.stop()


def create_bot():
    """
    helper function to easily create a bot for tests
    """
    bot_name = 'testBot'
    tokens = get_tokens()
    data = {
        'bot_name': bot_name,
        'bot_class': 'LazyBot',
        'user_name': 'williams23emma',
        'passwd': 'testPass',
        'access_token_key': os.environ['ACCESS_TOKEN_KEY_EMMA'],
        'access_token_secret': os.environ['ACCESS_TOKEN_SECRET_EMMA'],
        'consumer_key': os.environ['CONSUMER_KEY_EMMA'],
        'consumer_secret': os.environ['CONSUMER_SECRET_EMMA'],
    }
    response = requests.post(base_url + '/create_bot/', json=data)
    return response


def delete_bot():
    """
    helper function to easily delete a bot after tests
    """
    TwitterBotModel.delete().where(
        TwitterBotModel.bot_name == "testBot").execute()


class ServerThread(Thread):
    """
    Class to have a separate server thread in unittest to prevent blocking
    """

    def __init__(self, bind_address, app, name='Twitter Bot Control Server'):
        Thread.__init__(self)
        self.bind_address = bind_address
        self.app = app
        self.server_name = name
        self.server = WSGIServer(
            self.bind_address, self.app, server_name=self.server_name)

    def run(self):
        self.server.start()

    def stop(self):
        self.server.stop()


# def test_database(http_server):
# print(UnitTestConfig.DATABASE)


def test_home_http_200(http_server):
    """
    On successful request to '/'
    http status should be responded
    """
    resp = requests.get(base_url)
    assert resp.status_code == 200


# there is no redirect on bot creation anymore
#
# def test_create_bot_redirect200(http_server):
#     """
#     On successful submit of the form the user
#     should be redirected to localhost:5000/
#     an get a http status code 200
#     """
#     try:
#         resp = create_bot()
#     except Exception:
#         assert 1 == 0
#     finally:
#         delete_bot()
#     assert resp.status_code == 200
#     if 'H' in resp.text:
#         assert True


def test_create_bot_in_database():
    """
    On successful creation of a bot there should be
    an db entry for this bot
    """
    try:
        # resp = create_bot()
        # assert resp.status_code == 200
        testBot = TwitterBotModel.get(TwitterBotModel.bot_name == 'testBot')
        assert testBot is not None
    except Exception as e:
        print(e.args)
    finally:
        delete_bot()


def test_bot_subpage200(http_server):
    """
    On successful creation of a bot it should be possible to
    request the site of the bot. If the bot doesn't exist it
    shouldn't be possible to request its page
    """
    try:
        create_bot()
        test_bot = TwitterBotModel.get(TwitterBotModel.bot_name == "testBot")
        b_hash = test_bot.bot_hash
        resp = requests.get(base_url + '/bot/' + b_hash)
        assert resp.status_code == 200
        if test_bot.bot_name in resp.text:
            assert True
    except Exception as e:
        print(e.args)
    finally:
        delete_bot()
    failed_resp = requests.get(base_url + '/bot/000000000000')
    assert failed_resp.status_code == 500


def test_add_rule200(http_server):
    """
    On successful rule creation the api should return
    http status code 200
    """
    try:
        create_bot()
        test_bot = TwitterBotModel.get(TwitterBotModel.bot_name == "testBot")
        sample_rule = 'if tweet contains "hello world" then "bye python"'
        data = {'rule': sample_rule, 'bot_id': test_bot.id}
        resp = requests.post(base_url + '/save_rule/', json=data)
        assert resp.status_code == 200
        resp_json = json.loads(resp.text)
        rule_id = resp_json['rule_id']
        delete_rule_from_db(rule_id)
    except Exception as e:
        print(e.args)
    finally:
        delete_bot()


def test_rule_add_wrong_format(http_server):
    try:
        create_bot()
        test_bot = TwitterBotModel.get(TwitterBotModel.bot_name == "testBot")
        sample_rule = 'if contains "hello world" "bye python"'
        data = {'rule': sample_rule, 'bot_id': test_bot.id}
        resp = requests.post(base_url + '/save_rule/', json=data)
        json_resp = resp.json()
        assert resp.status_code == 400
        assert json_resp['message'] == 'rule is in wrong format'
    except Exception as e:
        print(e.args)
    finally:
        delete_bot()


def test_rule_deletion(http_server):
    try:
        create_bot()
        test_bot = TwitterBotModel.get(TwitterBotModel.bot_name == "testBot")
        sample_rule = 'if tweet contains "hello world" then "bye python"'
        data = {'rule': sample_rule, 'bot_id': test_bot.id}
        resp_add = requests.post(base_url + '/save_rule/', json=data)
        json_resp = resp_add.json()
        pprint(json_resp)
        rule_id = json_resp['rule_id']
        resp_del = requests.post(
            base_url + '/delete_rule/', json={
                'rule_id': rule_id
            })
        json_resp = resp_del.json()
        assert resp_del.status_code == 200
        assert json_resp['message'] == 'rule successfully deleted'
    except Exception as e:
        print(e.args)
    finally:
        pass


def test_bot_deletion_redirect(http_server):
    create_bot()
    test_bot = TwitterBotModel.get(TwitterBotModel.bot_name == "testBot")
    data = {'bot_id': test_bot.id}
    resp = requests.post(base_url + '/delete_bot/', data=data)
    assert resp.status_code == 200
    assert 'Home' in resp.text


def test_bot_deletion_from_db():
    """
    On success the bot should be deleted from the db
    and the resultset should be empty
    """
    create_bot()
    test_bot = TwitterBotModel.select().where(
        TwitterBotModel.bot_name == "testBot").get()
    assert test_bot is not None
    delete_bot()
    assert not TwitterBotModel.select().where(
        TwitterBotModel.bot_name == "testBot").exists()


# can't be tested for the moment because bot doesn't terminate quickly

# def test_start_stop_bot():
#     try:
#         create_bot()
#         test_bot = TwitterBotModel.get(TwitterBotModel.bot_name == "testBot")
#         print(test_bot.type_ref)
#         sample_rule = 'if tweet contains "hello world" then "bye python"'
#         data = {'rule': sample_rule, 'bot_id': test_bot.id}
#         rule_resp = requests.post(base_url + '/save_rule/', json=data)
#         start_resp = requests.post(
#             base_url + '/start_bot/', json={
#                 'bot_id': test_bot.id
#             })
#         assert start_resp.status_code == 200
#         data = {
#             'min_resp_t': 99,
#             'max_resp_t': 100,
#             'routine_t': 100,
#             'retweeted_sleep': 100,
#             'follower_sleep': 100,
#             'd_message_sleep': 100,
#             'bot_id': test_bot.id
#         }
#         ch_resp = requests.post(base_url + '/ch_bot_settings/', json=data)
#         assert ch_resp.status_code == 200
#         stop_resp = requests.post(
#             base_url + '/stop_bot/', json={
#                 'bot_id': test_bot.id
#             })
#         assert stop_resp.status_code == 200
#     except Exception as e:
#         print(e.args)
#     finally:
#         delete_bot()
#         resp_json = json.loads(rule_resp.text)
#         rule_id = resp_json['rule_id']
#         delete_rule_from_db(rule_id)
