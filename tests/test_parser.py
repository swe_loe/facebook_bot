from facebookbot.utils.rule_parser import SimpleParser
from pytest import raises
from pyparsing import ParseException


def test_simple_parse_sentence_success():
    """
    On successful parsing of multiple triggers or actions the
    parser should return a dict a key for triggers and actions.
    The values associated to the keys need to be strings
    """
    sample_rule = '''if tweet contains "this is a sample trigger"
    then "this is a sample reaction"'''
    parser = SimpleParser()
    result = parser.parse(sample_rule)
    triggers = result['triggers']
    actions = result['actions']
    assert triggers == 'this is a sample trigger'
    assert actions == 'this is a sample reaction'


def test_simple_parse_single_word_success():
    """
    If just a single word is given as trigger or reaction,
    then the values of the returned dictionary should only
    contain a string whithout whitespaces
    """
    sample_rule = 'if tweet contains "trigger" then "reaction"'
    parser = SimpleParser()
    result = parser.parse(sample_rule)
    triggers = result['triggers']
    actions = result['actions']
    assert triggers == 'trigger'
    assert actions == 'reaction'


def test_simple_parse_with_mixed_case_keywords():
    """
    Keywords can be specified in uppercases lowercases or mixedcase
    """
    sample_rule = '''iF twEet conTains "this is a sample trigger"
    THEN "this is a sample reaction"'''
    parser = SimpleParser()
    result = parser.parse(sample_rule)
    assert result['triggers'] == 'this is a sample trigger'
    assert result['actions'] == 'this is a sample reaction'


def test_simple_exception_wrong_rule_format():
    """
    if the rule is in the wrong format a ParseException should
    be raised
    """
    with raises(ParseException):
        wrong_format = '''if contains "this" then "that"'''
        parser = SimpleParser()
        parser.parse(wrong_format)
