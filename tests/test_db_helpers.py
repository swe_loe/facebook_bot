import requests
from facebookbot.utils.db_helpers import (save_rule_to_db, delete_rule_from_db,
                                          save_user)
from facebookbot.models import (
    BotType, Trigger, TwitterBotModel, TriggerAndAnswer, Answer, User, Tweet,
    Mention, Rule, Botrule, Retweeted, Favorited, Friend, Follower,
    StatisticsLog, Favorite, Reply, Retweet, Triggertweet)

base_url = 'http://localhost:5000'


def create_bot():
    """
    helper function to easily create a bot for tests
    """
    bot_name = 'testBot'
    data = {
        'bot_name': bot_name,
        'bot_class': 'LazyBot',
        'user_name': 'testUser',
        'passwd': 'testPass',
        'access_token_key': '986515396990775296-cTVvKv5s20TGRnXb9nH46kEnC8dvQVc',
        'access_token_secret': '4qQ3sqRsvTEQSbJsPA8MZTxCndDRmCbzT10pimzq5hAgD',
        'consumer_key': 'L76kfAYtRn4OTjl6UAhMQkMvx',
        'consumer_secret': 'dN087bq8tlvW1eIKkbyz4v5d3gHS1fRdI1O54iEhRPXKkGxI41',
    }
    response = requests.post(base_url + '/create_bot/', data=data)
    return response


def test_save_rule_to_db():
    """
    On successful saving of rule there should be an entry
    in the TriggerAndAnswer table corresponding to the rule
    """
    try:
        create_bot()
        bot = TwitterBotModel.get()
        rule_dict = {
            'bot_id': bot.id,
            'triggers': 'hello',
            'actions': 'bye',
            'rule': 'if tweet contains "hello" then "bye"'
        }
        trigger_and_answer_id = save_rule_to_db(rule_dict)
        assert trigger_and_answer_id is not None
        assert trigger_and_answer_id == 1
    finally:
        TwitterBotModel.delete_by_id(bot.id)

        delete_rule_from_db(trigger_and_answer_id)


def test_save_user():
    try:
        create_bot()
        user_dict = {}
        user_dict['user_id'] = 222222222222222222
        user_dict['screen_name'] = "test_user"
        user_dict['location'] = "test_location"
        user_dict['followers_count'] = 45
        user_dict['friends_count'] = 1890
        user_dict['created_at'] = '2018-05-21T10:59:42.145422'
        user_dict['favourites_count'] = '666'
        user_dict['verified'] = True
        user_dict['statuses_count'] = '777'
        user_dict['following'] = '888'

        save_user_id = save_user(user_dict)
        assert save_user_id is not None
        assert save_user_id == 1
    finally:
        TwitterBotModel.delete_by_id(bot.id)
