from logging.handlers import RotatingFileHandler
from os import makedirs
from os.path import join, exists
from pathlib import Path
import logging

LOGGING_ON = True
LOG_DIR = join(str(Path.home()), 'facebookbot_data/logs')
LEVEL = logging.INFO
FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
MAX_LOG_SIZE = 5  # MB

try:
    makedirs(LOG_DIR)
except OSError:
    pass


def __handler(module):
    return [
        RotatingFileHandler(
            filename='{}/{}.log'.format(LOG_DIR, module),
            maxBytes=MAX_LOG_SIZE * 1024 * 1024,
            backupCount=1)
    ]


def log(module, msg):
    """
    For logging manually
    """
    if LOGGING_ON:
        logging.basicConfig(
            handlers=__handler(module), level=LEVEL, format=FORMAT)

        logging.info(msg)


def for_all_methods(decorator):
    def decorate(cls):
        for attr in cls.__dict__:
            if callable(getattr(cls, attr)) and attr is not '__init__':
                setattr(cls, attr, decorator(getattr(cls, attr)))
        return cls

    return decorate


def logger(funct):
    """
    Decorater function for logging
    """
    if LOGGING_ON:
        logging.basicConfig(
            handlers=__handler(funct.__module__), level=LEVEL, format=FORMAT)

        def wrapper(*args, **kwargs):
            try:
                logging.info('Running {} with arguments: {} , {}'.format(
                    funct.__name__, args, kwargs))
            except:
                logging.info('Some information could not have been logged')
            return funct(*args, **kwargs)

        return wrapper
    else:
        return funct
