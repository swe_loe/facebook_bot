from requests import get, HTTPError
from pathlib import Path
from os.path import join, exists
from os import makedirs


def download_media(url, media_type, bot_name):
    """
    this function is for downloading media files,
    contained in tweets. It saves the files
    according to their filetype in a subdiriectory
    of the users home directory
    """
    parent_dir = 'facebookbot_data'
    home = Path.home()
    media_dir = join(home, parent_dir, bot_name, media_type)
    if not exists(media_dir):
        makedirs(media_dir)
    _, _, fname = url.rpartition('/')
    stor_loc = join(media_dir, fname)
    with get(url, stream=True) as r:
        if r.status_code == 200:
            with open(stor_loc, 'wb') as f:
                for chunk in r.iter_content(chunk_size=1024):
                    f.write(chunk)
        else:
            raise HTTPError
    return stor_loc
