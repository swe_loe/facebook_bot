import csv
from os import listdir, makedirs
from os.path import isfile, join, dirname, realpath, split, exists
import ast
from datetime import datetime, timedelta, timezone
from bson import json_util
from json import dumps, loads
from peewee import fn
from playhouse.shortcuts import model_to_dict
from pathlib import Path
from os.path import join, exists
from os import makedirs
from pprint import pprint
from facebookbot.utils.logger import log, logger
from facebookbot.models import (
    BotType, Trigger, TwitterBotModel, Answer, User, Tweet, Mention, Rule,
    Botrule, Retweeted, Friend, Follower, StatisticsLog, Favorite, Reply,
    Retweet, Triggertweet, DefaultSetting, Favorited)


# Rule
@logger
def save_rule_to_db(bot_id, rule_dict):
    """
    Params: bot_id: int
            rule_dict: dict
    If not existing, inserts new trigger and answer in the corresponding table
    If not existing, inserts new rule, else updates Rule-table
    Returns int
    """
    # bot_id, rule, triggers, actions):
    old_answer = None
    answer_id = None
    try:
        old_answer = Answer.get(Answer.answer == rule_dict['actions'])
        answer_id = old_answer.id
    except:
        pass
    new_answer = None
    if (old_answer is None):
        new_answer = Answer.create(answer=rule_dict['actions'], an_trig_ref=1)
        answer_id = new_answer.id

    old_trigger = None
    trigger_id = None
    try:
        old_trigger = Trigger.get(Trigger.trigger == rule_dict['triggers'])
        trigger_id = old_trigger.id
    except:
        pass
    new_trigger = None
    if (old_trigger is None):
        new_trigger = Trigger.create(
            trigger=rule_dict['triggers'], an_trig_ref=1)
        trigger_id = new_trigger.id

    new_rule = None
    try:
        new_rule = Rule.get((Rule.trigger_ref == trigger_id) &
                            (Rule.answer_ref == answer_id))
    except:
        pass
    if (new_rule is None):
        new_rule = Rule(trigger_ref=trigger_id, answer_ref=answer_id)
    new_rule.save()

    if bot_id is not None:
        new_botrule = None
        try:
            new_botrule = Botrule.get((Botrule.bot_ref == bot_id) &
                                      (Botrule.rule_ref == new_rule.id))
        except:
            pass
        if (new_botrule is None):
            new_botrule = Botrule(bot_ref=bot_id, rule_ref=new_rule.id)
        new_botrule.save()

    return new_rule.id


@logger
def delete_rule_from_db(bot_id, rule_id):
    """
    Params: rule_id: int
    Deletes record from Rule-table
    Returns int
    """
    try:
        return Botrule.delete().where((Botrule.bot_ref == bot_id) &
                                      (Botrule.rule_ref == rule_id)).execute()
    except Exception as ex:
        log(__name__, 'ERROR in delete_rule_from_db_2: {}'.format(ex))
        return -1


@logger
def get_trigger_by_rule(rule_id):
    """
    Params: rule_id: int
    Gets the trigger associated with a rule
    Returns string
    """
    try:
        rule = Rule.get(Rule.id == rule_id)
        trigger = Trigger.get(Trigger.id == rule.trigger_ref)
    except Exception as ex:
        log(__name__, 'ERROR in get_trigger_by_rule: {}'.format(ex))
        return ""
    return trigger.trigger


@logger
def get_answer_by_rule(rule_id):
    """
    Params: rule_id: int
    Gets the answer associated with a rule
    Returns string
    """
    try:
        rule = Rule.get(Rule.id == rule_id)
        answer = Answer.get(Answer.id == rule.answer_ref)
    except Exception as ex:
        log(__name__, 'ERROR in get_answer_by_rule: {}'.format(ex))
        return ""

    return answer.answer


@logger
def get_rules_from_db(bot_id):
    """
    Params: bot_id: int
    Gets the rules associated with a bot
    Returns dict
    """
    result = {}
    try:
        botrules = Botrule.select().where(Botrule.bot_ref == bot_id).execute()
        for botrule in botrules:
            Rule.get(Rule.id == botrule.rule_ref)
            trigger = get_trigger_by_rule(botrule.id)
            answer = get_answer_by_rule(botrule.id)
            result[trigger] = answer
    except Exception as ex:
        log(__name__, 'ERROR in get_rules_from_db: {}'.format(ex))
        return {}

    return result


@logger
def get_rules_with_id(bot_id):
    """
    this function is needed for the gui to have a reference to the
    rule (rule_id). This function should be merged with get_rules_from_db
    in the future
    """
    result = {}
    try:
        botrules = Botrule.select().where(Botrule.bot_ref == bot_id).execute()
        for botrule in botrules:
            rule = Rule.get(Rule.id == botrule.rule_ref)
            trigger = get_trigger_by_rule(rule.id)
            answer = get_answer_by_rule(rule.id)
            result[rule.id] = (trigger, answer)
    except Exception as ex:
        log(__name__, 'ERROR in get_rules_from_db: {}'.format(ex))
        return {}

    return result


@logger
def get_botrules_json(bot_id):
    try:
        select = Botrule.select().where(Botrule.bot_ref == bot_id)

        rules_dict = dumps(
            {
                "rows": [model_to_dict(c) for c in select]
            },
            default=json_util.default)

        return loads(rules_dict, object_hook=json_util.object_hook)
    except Exception as ex:
        log(__name__, 'ERROR in get_botrules_json: {}'.format(ex))
        return dumps({"rows": []}, default=json_util.default)


@logger
def get_all_rules():
    """
    Get all the rules out of the Rule table.
    Return: JSON
    """
    try:
        select = Rule.select()

        rules_dict = dumps(
            {
                "rows": [model_to_dict(c) for c in select]
            },
            default=json_util.default)

        return loads(rules_dict, object_hook=json_util.object_hook)

    except Exception as ex:
        log(__name__, 'ERROR in get_all_rules: {}'.format(ex))
        return dumps({"rows": []}, default=json_util.default)


@logger
def add_bot(bot_dict, user_dict):
    """
    Params: bot_dict: dict
    Adds a bot to the database
    Returns int
    """
    existing_bot = None
    try:
        existing_bot = TwitterBotModel.get(
            TwitterBotModel.bot_hash == bot_dict['bot_hash'])
    except:
        pass

    select = DefaultSetting.get_by_id(1)
    default_settings = model_to_dict(select)
    if (existing_bot is None):
        save_user(user_dict)
        return TwitterBotModel.create(
            bot_name=bot_dict['bot_name'],
            bot_hash=bot_dict['bot_hash'],
            type_ref=bot_dict['type_ref'],
            user_ref=user_dict['id'],
            # settings_ref=settings_ref.id,
            user_name=bot_dict['user_name'],
            access_token_key=bot_dict['access_token_key'],
            access_token_secret=bot_dict['access_token_secret'],
            consumer_key=bot_dict['consumer_key'],
            consumer_secret=bot_dict['consumer_secret'],
            min_resp_t=default_settings['min_resp_t'],
            max_resp_t=default_settings['max_resp_t'],
            routine_t=default_settings['routine_t'],
            retweeted_sleep=default_settings['retweeted_sleep'],
            follower_sleep=default_settings['follower_sleep'],
            d_message_sleep=default_settings['d_message_sleep'],
        )


@logger
def get_bot(bot_id):
    """
    Params: bot_id: int
    Gets the Twitterbotmodel informations from db.
    Return: bot_dict: dictionary
    """

    bot_dict = {}
    try:
        select = TwitterBotModel.get_by_id(bot_id)
        bot_dict = model_to_dict(select)
    except Exception as ex:
        log(__name__, 'ERROR in get_bot: {}'.format(ex))
        return {}
    return bot_dict


@logger
def get_all_bots():
    """
    Selects all bots saved to the database.
    """
    try:
        select = TwitterBotModel.select()

        bots_dict = dumps(
            {
                "rows": [model_to_dict(c) for c in select]
            },
            default=json_util.default)

        bots_dict_json = loads(bots_dict, object_hook=json_util.object_hook)

    except Exception as ex:
        log(__name__, 'ERROR in get_all_bots: {}'.format(ex))
        return dumps({"rows": []}, default=json_util.default)

    return bots_dict_json


@logger
def insert_bottype():
    """
    Used in the init.py of the Flask to save bottypes to the
    Bottype table.
    """
    # Get the existing filenames from the database
    query = BotType.select().where(BotType.id >= 0)
    existing_bottypes = [table_entry.filename for table_entry in query]

    # Get the files from the userbots directory
    parent_dir, _ = split(dirname(realpath(__file__)))
    _, top_level_package = split(parent_dir)
    userbots_path = join(parent_dir, 'bot/userbots')
    parent, package_name = split(userbots_path)
    _, parent_package_name = split(parent)

    onlyfiles = [
        f for f in listdir(userbots_path) if isfile(join(userbots_path, f))
    ]
    bfset = set(existing_bottypes)

    # Compare files from directory and database to changes
    # -> finds the new botfiles in the directory and ignores __init__.py
    compared_files = [
        x for x in onlyfiles if x not in bfset and x != "__init__.py"
    ]

    if compared_files:
        for botfile in compared_files:
            """
            Gets the name of the new file and the class in the file
            and saves it to the database
            import_name needs to be in the form of userbots.FILENAME
            (without trailing .py) because this format is needed to
            import the bots in function make_bot of module
            facebookbot/bot/twitter_bot.py
            """
            import_name = top_level_package + '.' \
                          + parent_package_name + '.' \
                          + package_name + '.' \
                          + botfile.rstrip('.py')
            with open(join(userbots_path, botfile), mode='r') as f:
                parsed = ast.parse(f.read())
            found_class = [
                node.name
                for node in ast.walk(parsed)
                if isinstance(node, ast.ClassDef)
            ][0]
            bot = BotType.create(
                filename=botfile,
                import_name=import_name,
                bot_class=found_class)
            bot.save()


# User
@logger
def save_user(user_dict):
    """
    Params: user_dict: {
                        'id': int
                        'screen_name': string
                        'location': string
                        'followers_count': int
                        'created_at': date
                        'favourites_count': int
                        'verified': bool
                        'statuses_count': int
                       }
    Saves a nonexisting user to the database.

    """
    existing_user = None
    try:
        existing_user = User.get(User.user_id == user_dict['id'])
    except:
        pass

    if existing_user is None:
        try:
            return User.create(
                user_id=user_dict['id'],
                screen_name=user_dict['screen_name'],
                location=user_dict['location'],
                followers_count=user_dict['followers_count'],
                friends_count=user_dict['friends_count'],
                created_at=user_dict['created_at'],
                favourites_count=user_dict['favourites_count'],
                verified=user_dict['verified'],
                statuses_count=user_dict['statuses_count']
                # following=user_dict['following']
            )
        except Exception as ex:
            log(__name__, 'ERROR: Could not save user: {}'.format(ex))
            return -1


@logger
def get_user(uid):
    """
    Params: userid: int
    Get a specific user by their userid from the database.
    Return: user_dict:  {
                        'user_id': int
                        'screen_name': string
                        'location': string
                        'followers_count': int
                        'friends_count': int
                        'created_at': date
                        'favourites_count': int
                        'verified': bool
                        'statuses_count': int
                        'following': int
                        }
    """
    try:
        select = User.get_by_id(uid)
        user_dict = model_to_dict(select)

    except Exception as ex:
        log(__name__, 'ERROR in get_user: {}'.format(ex))
        return {}

    return user_dict


@logger
def get_all_users():
    """
    Returns a dictionary with all users saved in the database.
    """
    try:
        select = User.select()

        user_dict = dumps(
            {
                "rows": [model_to_dict(c) for c in select]
            },
            default=json_util.default)

    except Exception as ex:
        log(__name__, 'ERROR in get_all_users: {}'.format(ex))
        return dumps({"rows": []}, default=json_util.default)

    return loads(user_dict, object_hook=json_util.object_hook)


@logger
def delete_user(uid):
    # TODO: look if referenced somewhere else, if not delete it aswell -> delete_instance
    try:
        return User.delete_by_id(uid)

    except Exception as ex:
        log(__name__, 'ERROR in delete_user: {}'.format(ex))
        return -1


# Tweet
@logger
def save_tweet(bot_id, tweet_dict):
    """
    Params: bot_id: int
            tweet_dict: dictionary
    Saves a tweet to the database.
    bot_id only for download path.
    """
    existing_tweet = None
    try:
        existing_tweet = Tweet.get(Tweet.tweet_id == tweet_dict['id'])
    except:
        pass

    if (existing_tweet is None):
        try:
            parent_dir = 'facebookbot_data'
            home = Path.home()

            media_p = ''
            media_url_ = ''
            media_type_ = ''

            try:
                possibly_sensitive = tweet_dict['possibly_sensitive']
            except:
                possibly_sensitive = ''
            try:
                tweet_url = tweet_dict['urls'][0]['url']
            except:
                tweet_url = ''
            try:
                download_tweet(
                    TwitterBotModel.get_by_id(bot_id).bot_name, tweet_dict)
                media_url_ = tweet_dict['entities']['media'][0]['url']
                media_type_ = tweet_dict['entities']['media'][0]['type']
                media_p = download_media(bot_id, tweet_dict)
            except:
                pass

            return Tweet.create(
                tweet_id=tweet_dict['id'],
                user_ref=tweet_dict['user']['id'],
                created_at=tweet_dict['created_at'],
                text=tweet_dict['text'],
                url=tweet_url,
                media_url=media_url_,
                media_type=media_type_,
                in_reply_to_status_id=tweet_dict['in_reply_to_status_id'],
                in_reply_to_status_id_str=tweet_dict[
                    'in_reply_to_status_id_str'],
                place=tweet_dict['place'],
                retweet_count=tweet_dict['retweet_count'],
                favorite_count=tweet_dict['favorite_count'],
                favorited=tweet_dict['favorited'],
                retweeted=tweet_dict['retweeted'],
                possibly_sensitive=possibly_sensitive,
                media_path=media_p)
        except Exception as ex:
            log(__name__, 'ERROR could not save tweet: {}'.format(ex))
            return -1


@logger
def get_tweet(twid):
    """
    Params: TweetID: int
    Gets a specific tweet by its ID from the database.
    Return: tweet_dict: dictionary
    """
    try:
        select = Tweet.get_by_id(twid)
        tweet_dict = model_to_dict(select)
        return tweet_dict

    except Exception as ex:
        log(__name__, 'ERROR in get_tweet: {}'.format(ex))
        return {}


@logger
def get_all_tweets():
    """
    Gets all tweets saved to the database.
    Return: dictionary
    """
    try:
        select = Tweet.select()

        tweets_dict = dumps(
            {
                "rows": [model_to_dict(c) for c in select]
            },
            default=json_util.default)

    except Exception as ex:
        log(__name__, 'ERROR in get_all_tweets: {}'.format(ex))
        return dumps({"rows": []}, default=json_util.default)

    return loads(tweets_dict, object_hook=json_util.object_hook)


@logger
def delete_tweet(twid):
    """
    Delete a specific tweet by its ID.
    """

    try:
        return Tweet.delete_by_id(twid)
    except Exception as ex:
        log(__name__, 'ERROR in delete_tweet: {}'.format(ex))
        return -1


# Settings
@logger
def save_defaultsettings(set_dict=None):
    """
    Params: set_dict: dict
    Saves one set of default settings to the database.
    If the set_dict is not sent the record updates.
    Return: Model
    """
    exists = None
    try:
        exists = DefaultSetting.select().count()
    except:
        pass
    try:
        if exists <= 0 and set_dict is None:
            return DefaultSetting.create()

        elif exists > 0 and set_dict is not None:
            return DefaultSetting.update(
                min_resp_t=set_dict['min_resp_t'],
                max_resp_t=set_dict['max_resp_t'],
                routine_t=set_dict['routine_t'],
                retweeted_sleep=set_dict['retweeted_sleep'],
                follower_sleep=set_dict['follower_sleep'],
                d_message_sleep=set_dict['d_message_sleep']).where(
                    DefaultSetting.id == 1).execute()
    except Exception as ex:
        log(__name__, 'ERROR in get_defaultsettings: {}'.format(ex))
        return {}


@logger
def get_defaultsettings(set_id):
    """
    Gets the default settings out of the DefaultSettings table.
    """

    try:
        select = DefaultSetting.get_by_id(set_id)
        set_dict = model_to_dict(select)
        return set_dict
    except Exception as ex:
        log(__name__, 'ERROR in save_defaultsettings: {}'.format(ex))
        return -1


@logger
def get_botsettings(bot_id):
    """
    Params: bot_id: int
    This method is there to get the settings for a specific bot.
    Because there are default settings we have to look through the
    settings values of the bot. If the settings value is not None
    the default value will be replaced.
    Return: Dict
    """
    try:

        def_select = DefaultSetting.select().join(TwitterBotModel).where(
            TwitterBotModel.settings_ref == bot_id).get()
        def_dict = model_to_dict(def_select)

        botset_select = TwitterBotModel.select().where(
            TwitterBotModel.id == bot_id).get()
        botset_dict = model_to_dict(botset_select)

        set_dict = {}

        for def_key in def_dict:
            for bot_key in botset_dict:
                if def_key == bot_key:
                    if botset_dict[bot_key] is not None:
                        set_dict[bot_key] = botset_dict[bot_key]
                    else:
                        set_dict[def_key] = def_dict[def_key]
        return set_dict

    except Exception as ex:
        log(__name__, 'ERROR in get_botsettings: {}'.format(ex))
        return {}


@logger
def update_botsettings(set_dict):
    # Updates settings to db
    try:
        update = TwitterBotModel.update(
            min_resp_t=set_dict['min_resp_t'],
            max_resp_t=set_dict['max_resp_t'],
            routine_t=set_dict['routine_t'],
            retweeted_sleep=set_dict['retweeted_sleep'],
            follower_sleep=set_dict['follower_sleep'],
            d_message_sleep=set_dict['d_message_sleep']).where(
                TwitterBotModel.id == set_dict['bot_id'])

    except Exception as ex:
        log(__name__, 'ERROR in update_botsettings: {}'.format(ex))
        return -1

    return update.execute()


# Statistics
#
@logger
def get_statistics(bot_id=None):
    """
    Params: bot_id: int
    If param is set, it gets the stat values of the bot with the given id,
    else it gets the values of all bots
    Returns dict
    """
    stats = None
    if (bot_id is not None):
        try:
            stats = StatisticsLog.select().where(
                StatisticsLog.bot_ref == bot_id).get()
        except:
            pass
    else:
        stats = StatisticsLog.select().get()
    try:
        result = model_to_dict(stats)
        return result
    except Exception as ex:
        log(__name__, 'ERROR: {}'.format(ex))
        return {}


@logger
def save_statistics(bot_id, user):
    """
    Params: bot_id int,
            user dict
    Saves/updates statistics for the bot with the given bot_id
    Returns int
    """
    bot_stat = None
    try:
        bot_stat = StatisticsLog.get(StatisticsLog.bot_ref == bot_id)
    except:
        pass
    if (bot_stat is None):
        try:
            return StatisticsLog.create(
                bot_ref=bot_id,
                followers=user['followers_count'],
                follows=user['friends_count'],
                tweets=user['statuses_count'],
                retweets=count_retweet(bot_id),
                retweeted=count_retweeted(bot_id),
                likes=user['favourites_count'],
                liked=0)  # cannot be counted
        except Exception as ex:
            log(__name__, 'ERROR: {}'.format(ex))
            return -1
    else:
        try:
            return StatisticsLog.update(
                bot_ref=bot_id,
                followers=user['followers_count'],
                follows=user['friends_count'],
                tweets=user['statuses_count'],
                retweets=count_retweet(bot_id),
                retweeted=count_retweeted(bot_id),
                likes=user['favourites_count'],
                liked=0).where(StatisticsLog.bot_ref == bot_id).execute()
        except Exception as ex:
            log(__name__, 'ERROR: {}'.format(ex))
            return -1


# Mentions
@logger
def save_mentions(bot_id, mention_dict):
    """
    Params: bot_id: int
            mention_dict: Userdictionary
    Saves the mentions made in a tweet.
    """
    try:
        save_tweet(bot_id, mention_dict)
        return Mention.create(tweet_ref=mention_dict['id'], bot_ref=bot_id)

    except Exception as ex:
        log(__name__, 'ERROR in save_mentions: {}'.format(ex))
        return -1


@logger
def delete_mention(ment_id):
    """
    Deletes a specific mention by its ID out if the Mention table.
    """
    try:
        return Mention.delete_by_id(ment_id)

    except Exception as ex:
        log(__name__, 'ERROR in delete_mention: {}'.format(ex))
        return -1


@logger
def get_all_mentions(bot_id):
    """
    Get all mentions of a specific bot.
    """

    try:
        # select = Retweeted.select().where(Retweeted.bot_ref == bot_id)
        # retweeted_dict = [model_to_dict(c) for c in select]
        select = Mention.select().where(Mention.bot_ref == bot_id)
        mention_dict = [model_to_dict(c) for c in select]
        return mention_dict

    except Exception as ex:
        log(__name__, 'ERROR in delete_mention: {}'.format(ex))
        return []


# Retweeted
@logger
def save_retweeted(bot_id, retweeted_dict):
    """
    Saves the bot and tweet foreignkey which were retweeted
    """

    existing_retweet = None
    try:
        existing_retweet = Retweeted.select().join(Tweet).where(
            Retweeted.tweet_ref == retweeted_dict['id']
            and Retweeted.bot_ref == bot_id).get()
    except Exception as ex:
        pass

    try:
        if existing_retweet is None:
            # No need to save the tweet here, since they are our own tweets.
            # (unless we did not save our tweet, being offline, and someone retweeted our tweet now!)
            save_tweet(bot_id, retweeted_dict)
            return Retweeted.create(
                tweet_ref=retweeted_dict['id'], bot_ref=bot_id)
    except Exception as ex:
        log(__name__, 'ERROR in save_retweeted: {}'.format(ex))
        return -1


@logger
def delete_retweeted(retweeted_id):
    try:
        return Retweeted.delete_by_id(retweeted_id)

    except Exception as ex:
        log(__name__, 'ERROR in delete_retweeted: {}'.format(ex))
        return -1


@logger
def get_all_retweeted(bot_id):
    try:

        select = Retweeted.select().where(Retweeted.bot_ref == bot_id)
        retweeted_dict = [model_to_dict(c) for c in select]

    except Exception as ex:
        log(__name__, 'ERROR in get_all_retweeted: {}'.format(ex))
        return []

    return retweeted_dict


@logger
def get_latest_retweeted(bot_id):
    """
    Params: bot_id: int
    Gets the latest Retweeted for a specific bot
    Return: tweet_id: int
    """

    try:
        select = (Retweeted.select(Retweeted, fn.MAX(
            Retweeted.created)).where(Retweeted.bot_ref == bot_id)).get()

        retweeted_dict = model_to_dict(select)
        return retweeted_dict['tweet_ref']['tweet_id']

    except Exception as ex:
        log(__name__, 'ERROR in get_latest_retweeted: {}'.format(ex))
        return None


# Favorited
# not in use
@logger
def save_favorited(bot_id, favorited_dict):

    existing_favorited = None
    try:
        existing_favorited = Favorited.select().join(Tweet).where(
            Favorited.tweet_ref == favorited_dict['id']
            and Favorited.bot_ref == bot_id).get()
    except Exception as ex:
        pass

    if (existing_favorited is None):
        save_tweet(bot_id, favorited_dict)
        return Favorited.create(tweet_ref=favorited_dict['id'], bot_ref=bot_id)

    # return Favorite.create(
    #     favd_id=favorited_dict['favorite_id'],
    #     tweet_ref=favorited_dict['tweet_id'],
    #     bot_ref=favorited_dict['bot_id'])


@logger
def delete_favorited(favorited_id):

    return Favorite.delete_by_id(favorited_id).execute()


@logger
def get_all_favorited(bot_id):

    select = Favorite.select().where(Favorite.bot_ref == bot_id)

    favorited_dict = [model_to_dict(c) for c in select]

    return favorited_dict


@logger
def get_favorited(tweet_id):

    try:
        select = Favorite.select().where(Favorite.tweet_ref == tweet_id)

        favorited_dict = dumps(
            {
                "rows": [model_to_dict(c) for c in select]
            },
            default=json_util.default)

        favorited_dict_json = loads(
            favorited_dict, object_hook=json_util.object_hook)

        return favorited_dict_json
    except:
        return None


def save_friend(friend_dict, bot_id):

    existing_friend = None
    try:

        existing_friend = Friend.get((Friend.user_ref == friend_dict['id']) &
                                     (Friend.bot_ref == bot_id))

    except Exception as ex:
        pass

    try:

        if (existing_friend is None):

            save_user(friend_dict)
            return Friend.create(user_ref=friend_dict['id'], bot_ref=bot_id)
    except Exception as ex:
        log(__name__, 'ERROR could not save friend: {}'.format(ex))
        return -1


@logger
def delete_friend(frid):
    """Params: friend_id: int
    Deletes the friend with the given id from the database.
    Return: Delete Return
    """
    try:
        return Friend.delete_by_id(frid)

    except Exception as ex:
        log(__name__, 'ERROR in delete_friend: {}'.format(ex))
        return -1


@logger
def get_all_friends(bot_id):
    """Params: bot_id: int
       Gets all friends for a specific bot from the database
       Return: friend_dict: dictionary
    """
    try:
        select = Friend.select().where(Friend.bot_ref == bot_id)

        friend_dict = dumps(
            {
                "rows": [model_to_dict(c) for c in select]
            },
            default=json_util.default)

    except Exception as ex:
        log(__name__, 'ERROR in get_all_friends: {}'.format(ex))
        return dumps({"rows": []}, default=json_util.default)

    return friend_dict


# Follower
@logger
def save_follower(bot_id, follow_dict):
    existing_follower = None
    try:
        existing_follower = Follower.get(
            (Follower.user_ref == follow_dict['id']) &
            (Follower.bot_ref == bot_id))
    except Exception as ex:
        pass

    try:
        if (existing_follower is None):
            save_user(follow_dict)
            return Follower.create(user_ref=follow_dict['id'], bot_ref=bot_id)

    except Exception as ex:
        log(__name__, 'ERROR could not save follower: {}'.format(ex))
        return -1


@logger
def delete_follower(foid):
    #TODO: check if the user object is referenced anywhere else, if not delete it aswell.
    return Follower.delete_by_id(foid)


@logger
def get_all_follower(bot_id):

    try:
        select = Follower.select().where(Follower.bot_ref == bot_id)
        follower_dict = [model_to_dict(c) for c in select]

    except Exception as ex:
        log(__name__, 'ERROR in get_all_follower: {}'.format(ex))
        return []

    return follower_dict


@logger
def get_latest_follower(bot_id):
    """
    Param: bot_id: int
    Gets the latest Follower for a specific bot
    Return: user_id: int
    """
    try:
        select = (Follower.select(Follower, fn.MAX(
            Follower.created)).where(Follower.bot_ref == bot_id)).get()

        follower_dict = model_to_dict(select)

        return follower_dict['user_ref']['user_id']
    except Exception as ex:
        log(__name__, 'ERROR in get_latest_follower: {}'.format(ex))
        return None


# Records
@logger
def get_records_stats(bot_id, record_type, weeks):
    """
    Params: bot_id: int
            record_type: string
            weeks: int
    Gets the amount of followers/retweets/favorited for each of the last weeks
    Returns dict
    """
    #convert bot_creation_time to utc, since we need to make it compatible with the twitter-utc-timestamps
    #change offset of 2h to Berlin time
    bot_creation_time = get_bot(bot_id)['created'].replace(
        tzinfo=timezone(timedelta(seconds=+7200)))

    last_records = {}
    all_table_records = {}

    if (record_type == "follower"):
        all_table_records = get_all_follower(bot_id)
    elif (record_type == "mentions"):
        all_table_records = get_all_mentions(bot_id)
    elif (record_type == "retweeted"):
        all_table_records = get_all_retweeted(bot_id)
    elif (record_type == "tweets"):
        user_id = get_bot(bot_id)['user_ref']['user_id']
        all_table_records = get_user_tweet(user_id)
    record_date = None
    date_now = datetime.now(timezone.utc)

    for week in range(0, -weeks, -1):

        last_days = week * (-7)
        amount_records = 0
        date_check = date_now - timedelta(days=last_days)

        if bot_creation_time > date_check:
            break

        for table_record in all_table_records:
            try:
                #record_date and date_check should be in the same format
                record_date = table_record['created'].replace(
                    tzinfo=timezone(timedelta(seconds=+7200)))
            except:
                record_date = datetime.strptime(
                    str(table_record['created_at']), '%a %b %d %H:%M:%S %z %Y')
            if record_date <= date_check:
                amount_records += 1

        last_records[-week] = amount_records
    return last_records


# Favorite
@logger
def save_favorite(bot_id, tweet, rule_key):
    """
    Saves a favorite made by the bot to the database
    Return: Model
    """
    exists = None
    tweet_id = tweet['id']
    rule_id = get_rule_id(rule_key, bot_id)
    try:
        exists = Favorite.select().where(Favorite.tweet_ref == tweet_id
                                         and Favorite.rule_ref == rule_id
                                         and Favorite.bot_ref == bot_id).get()
    except:
        pass

    if exists is None:
        try:
            save_tweet(bot_id, tweet)
            return Favorite.create(
                tweet_ref=tweet_id, rule_ref=rule_id, bot_ref=bot_id)
        except Exception as ex:
            log(__name__, 'ERROR could not save favorite: {}'.format(ex))
            return -1


@logger
def delete_favorite(fav_id):
    """
    Deletes a favorite entry by its id.
    """
    # TODO: check if tweet with id 'fav_id' is referenced anywhere else, if not delete it aswell.
    try:
        Favorite.delete_by_id(fav_id)
    except Exception as ex:
        log(__name__, 'ERROR in delete_favorite: {}'.format(ex))
        return -1


# Reply
@logger
def save_reply(bot_id, tweet, rule_key):
    """
    Saves a reply made by the bot to the database
    Return: Model
    """
    tweet_id = tweet['id']
    rule_id = get_rule_id(rule_key, bot_id)
    replied_tweet_id = tweet['in_reply_to_status_id']
    exists = None
    try:
        exists = Reply.select().where(
            Reply.tweet_ref == tweet_id and Reply.rule_ref == rule_id
            and Reply.replied_status_ref == replied_tweet_id).get()
    except:
        pass

    if exists is None:
        try:
            save_tweet(bot_id, tweet)
            return Reply.create(
                tweet_ref=tweet_id,
                rule_ref=rule_id,
                replied_status_ref=replied_tweet_id)

        except Exception as ex:
            log(__name__, 'ERROR in save_reply: {}'.format(ex))
            return -1


@logger
def delete_reply(reply_id):
    """
    Deletes a reply entry by its id.
    """
    # TODO: check if tweet with 'reply_id' is referenced anywhere else, if not delete it as well.
    try:
        Reply.delete_by_id(reply_id)
    except Exception as ex:
        log(__name__, 'ERROR in delete_reply: {}'.format(ex))
        return -1


# Retweet
@logger
def save_retweet(bot_id, tweet, rule_key):
    """
    Saves a retweet made by the bot to the database
    Return: Model
    """
    tweet_id = tweet['id']
    retweeted_tweet_id = tweet['retweeted_status']['id']
    bot = TwitterBotModel.get_by_id(bot_id)
    rule_id = get_rule_id(rule_key, bot_id)
    exists = None
    try:
        exists = Retweet.select().where(
            Retweet.tweet_ref == tweet_id and Retweet.rule_ref == rule_id
            and Retweet.retweeted_status_ref == retweeted_tweet_id).get()
    except:
        pass

    try:
        if exists is None:
            save_tweet(bot_id, tweet)
            return Retweet.create(
                tweet_ref=tweet_id,
                rule_ref=rule_id,
                retweeted_status_ref=retweeted_tweet_id)
    except Exception as ex:
        log(__name__, 'ERROR in save_retweet: {}'.format(ex))
        return -1


@logger
def delete_retweet(retweet_id):
    try:
        Retweet.delete_by_id(retweet_id)
    except Exception as ex:
        log(__name__, 'ERROR in delete_retweet: {}'.format(ex))
        return -1


@logger
def get_retweet(bot_id, tweet_id):
    """Params: bot_id: int
               tweet_id: int
    """
    try:
        select = (Retweet.select(Retweet).join(Rule).join(Botrule).join(
            TwitterBotModel).where(Botrule.bot_ref == bot_id
                                   and Retweet.tweet_ref == tweet_id).get())

        tweet_dict = model_to_dict(select)
        return tweet_dict
    except Exception as ex:
        log(__name__, 'ERROR in get_retweet: {}'.format(ex))
        return {}


# Triggertweet
@logger
def save_triggertweet(bot_id, tweet, reply_id):
    """
    Params: tweet: tweet json
            reply_id: id (bot tweet, that replied to tweet)
    Makes it possible to save the tweet that triggered the bot
    Return: Model
    """
    tweet_id = tweet['id']

    exists = None
    try:
        exists = Triggertweet.select().where(
            Triggertweet.tweet_ref == tweet_id
            and Triggertweet.reply_ref == reply_id).get()
    except Exception as ex:
        pass

    if exists is None:
        try:
            save_tweet(bot_id, tweet)
            return Triggertweet.create(tweet_ref=tweet_id, reply_ref=reply_id)
        except Exception as ex:
            log(__name__, 'ERROR in save_triggertweet: '.format(ex))
            return -1


@logger
def get_user_tweet(user_id):
    user_tweets_dict = None

    try:
        select = Tweet.select().where(Tweet.user_ref == user_id)
        user_tweets_dict = [model_to_dict(c) for c in select]
    except Exception as ex:
        log(__name__, 'ERROR in get_user_tweet: {}'.format(ex))
        return []

    return user_tweets_dict


@logger
def delete_triggertweet(trtweet_id):
    try:
        return Triggertweet.delete_by_id(trtweet_id)
    except Exception as ex:
        log(__name__, 'ERROR in delete_triggertweet: {}'.format(ex))
        return -1


@logger
def get_followed_bot_by_user(user_id):
    try:
        followed = Follower.select().where(Follower.user_ref == user_id)
        follower_dict = dumps(
            {
                "rows": [model_to_dict(c) for c in followed]
            },
            default=json_util.default)

        follower_dict_json = loads(
            follower_dict, object_hook=json_util.object_hook)
        result = []

        for entry in follower_dict_json["rows"]:
            result.append(entry['bot_ref'])
        return result
    except Exception as ex:
        log(__name__, 'ERROR in get_followed_bot_by_user: {}'.format(ex))
        return []


@logger
def get_retweeted_bot(tweet_id):
    try:
        result = Retweeted.get(Retweeted.tweet_ref == tweet_id)
        bot_id = result.bot_ref_id
        bot = get_bot(bot_id)
        bot_name = bot['bot_name']
        return bot_name
    except Exception as ex:
        return ""


@logger
def get_retweeted_status(tweet_id):
    try:
        retweet = Retweet.select().where(
            Retweet.retweeted_status_ref == tweet_id)
        retweet_dict = dumps(
            {
                "rows": [model_to_dict(c) for c in retweet]
            },
            default=json_util.default)

        retweet_dict_json = loads(
            retweet_dict, object_hook=json_util.object_hook)
        return retweet_dict_json
    except:
        return None


@logger
def get_bot_from_user_id(user_id):
    try:
        bot = TwitterBotModel.select().where(
            TwitterBotModel.user_ref == user_id)
        bot_dict = dumps(
            {
                "rows": [model_to_dict(c) for c in bot]
            },
            default=json_util.default)

        bot_dict_json = loads(bot_dict, object_hook=json_util.object_hook)

        return bot_dict_json
    except:
        return None


# def get_friend(user_id, bot_id):
#     try:
#         select = Friend.select().where(Friend.user_ref == user_id and Friend.bot_ref == bot_id)
#         friend_dict = dumps(
#             {
#                 "rows": [model_to_dict(c) for c in select]
#             },
#             default=json_util.default)

#         return loads(friend_dict, object_hook=json_util.object_hook)
#     except:
#         return None

# def save_friend
"""
HELPER METHODS
"""


@logger
def get_tokens(bot_id):
    """
    Returns the token dictionary of bot_id
    """
    try:
        bot = TwitterBotModel.get_by_id(bot_id)
    except Exception as ex:
        log(__name__, 'ERROR in get_tokens: {}'.format(ex))
        return {}

    return {
        'access_token_key': bot.access_token_key,
        'access_token_secret': bot.access_token_secret,
        'consumer_key': bot.consumer_key,
        'consumer_secret': bot.consumer_secret
    }


@logger
def count_retweeted(bot_id):
    """
    Returns the amount of how many times all tweets from the bot have been retweeted
    if no such bot exists, return 0
    """
    try:
        res = Retweeted.select().where(Retweeted.bot_ref == bot_id).count()
    except Exception as ex:
        log(__name__, 'ERROR in count_retweeted: {}'.format(ex))
        return -1
    return res


@logger
def count_retweet(bot_id):
    """
    Returns the amount of retweets the bot made
    """
    try:
        bot = TwitterBotModel.get_by_id(bot_id)
        res = Retweet.select().join(
            Tweet, on=(Tweet.tweet_id == Retweet.tweet_ref
                       )).where(Tweet.user_ref == bot.user_ref).count()
    except Exception as ex:
        log(__name__, 'ERROR in count_retweet: {}'.format(ex))
        return -1
    return res


@logger
def get_rule_id(rule_key, bot_id):
    """
    Returns the rule_id from a given trigger word (rule key)
    Returns -1 if rule was not found and/or other exceptions
    """
    try:
        select = Rule.select().join(Trigger).switch(Rule).join(Botrule).join(
            TwitterBotModel).where(Trigger.trigger == rule_key
                                   and TwitterBotModel.id == bot_id).get()
        rule_dict = model_to_dict(select)
        return rule_dict['id']
    except Exception as ex:
        log(__name__, 'ERROR in get_rule_id: {}'.format(ex))
        return 1


@logger
def export_bots(date):
    export_dir = get_export_dir(date)
    path = join(export_dir, 'bots.csv')
    if not exists(path):
        open(path, 'x').close()

    entry = {}
    entry['bot_name'] = ""
    entry['type_ref'] = {}
    entry['type_ref']['bot_class'] = ""
    entry['user_ref'] = {}
    entry['user_ref']['screen_name'] = ""
    entry['user_ref']['followers_count'] = ""
    entry['user_ref']['friends_count'] = ""
    entry['user_ref']['statuses_count'] = ""
    entry['user_ref']['favourites_count'] = ""
    entry['min_resp_t'] = ""
    entry['max_resp_t'] = ""

    with open(path, 'w') as csv_file:
        csv_writer = csv.writer(csv_file)
        csv_writer.writerow([
            'bot_name', 'type', 'user_name', 'followers_count',
            'friends_count', 'tweets_count', 'retweet_count',
            'retweeted_count', 'favourites_count', 'favourited_count',
            'min_resp_t', 'max_resp_t', 'rules', 'created'
        ])
        dicts_json = get_all_bots()
        for entry in dicts_json["rows"]:
            rules = []
            botrules = get_botrules_json(entry['id'])
            for botrule in botrules["rows"]:
                rules.append(botrule['id'])
            stats = get_statistics(entry['id'])
            csv_writer.writerow([
                entry['bot_name'], entry['type_ref']['bot_class'],
                entry['user_ref']['screen_name'],
                entry['user_ref']['followers_count'],
                entry['user_ref']['friends_count'],
                entry['user_ref']['statuses_count'], stats['retweets'],
                stats['retweeted'], stats['likes'], stats['liked'],
                entry['min_resp_t'], entry['max_resp_t'], rules,
                entry['created']
            ])


@logger
def export_users(date):
    export_dir = get_export_dir(date)
    path = join(export_dir, 'users.csv')
    if not exists(path):
        open(path, 'x').close()

    entry = {}
    entry['screen_name'] = ""
    entry['location'] = ""
    entry['followers_count'] = ""
    entry['friends_count'] = ""
    entry['statuses_count'] = ""
    entry['favourites_count'] = ""
    entry['verified'] = ""
    entry['created_at'] = ""

    with open(path, 'w') as csv_file:
        csv_writer = csv.writer(csv_file)
        csv_writer.writerow([
            'user_name', 'location', 'followed_bot', 'followers_count',
            'friends_count', 'tweets_count', 'favourites_count', 'verified',
            'created'
        ])

        dicts_json = get_all_users()
        for entry in dicts_json["rows"]:
            followed_bots = []
            followed_bots_json = get_followed_bot_by_user(entry['user_id'])
            for followed_bot in followed_bots_json:
                followed_bots.append(followed_bot['bot_name'])

            csv_writer.writerow([
                entry['screen_name'], entry['location'], followed_bots,
                entry['followers_count'], entry['friends_count'],
                entry['statuses_count'], entry['favourites_count'],
                entry['verified'], entry['created_at']
            ])


@logger
def export_tweets(date):
    export_dir = get_export_dir(date)
    path = join(export_dir, 'tweets.csv')
    if not exists(path):
        open(path, 'x').close()

    entry = {}
    retweeted_bot = ""
    replied_tweet = {}
    entry['tweet_id'] = ""
    entry['text'] = ""
    entry['url'] = ""
    entry['media_url'] = ""
    entry['media_type'] = ""
    entry['media_path'] = ""
    entry['in_reply_to_status_id'] = ""
    entry['retweet_count'] = ""
    entry['favourite_count'] = ""
    entry['possibly_sensitive'] = ""
    entry['place'] = ""
    entry['created_at'] = ""
    entry['bot_name'] = ""
    entry['location'] = ""
    entry['user_ref'] = {}
    entry['user_ref']['screen_name'] = ""
    retweeted_tweet = {}
    favorited = {}
    replied_tweet['text'] = ""

    with open(path, 'w') as csv_file:
        csv_writer = csv.writer(csv_file)
        csv_writer.writerow([
            'tweet_id', 'user_name', 'text', 'url', 'media_url', 'media_type',
            'media_path', 'retweeted_bot', 'replied_tweet_id',
            'replied_tweet_text', 'retweet_count', 'favourite_count',
            'possibly_sensitive', 'place', 'retweeted_by_bots',
            'favourited_by_bot', 'created'
        ])

        dicts_json = get_all_tweets()
        for entry in dicts_json["rows"]:
            try:
                retweeted_bot = get_retweeted_bot(entry['tweet_id'])
                result = get_tweet(entry['in_reply_to_status_id'])
                if result:
                    replied_tweet = result
                retweeted_tweet = get_retweeted_status(entry['tweet_id'])
                retweeted_by_bots = []

                if retweeted_tweet["rows"]:
                    for row in retweeted_tweet["rows"]:
                        tripel = []
                        bot = get_bot_from_user_id(
                            row['tweet_ref']['user_ref']['user_id'])
                        bot_name = bot["rows"][0]['bot_name']
                        tripel.append(bot_name)
                        tripel.append(row['tweet_ref']['tweet_id'])
                        rule = row['rule_ref']['id']
                        tripel.append(rule)
                        retweeted_by_bots.append(tripel)

                favorited = get_favorited(entry['tweet_id'])
                favorited_by_bots = []
                if favorited["rows"]:
                    for row in favorited["rows"]:
                        tupel = []
                        bot_name = row['bot_ref']['bot_name']
                        tupel.append(bot_name)
                        rule = row['rule_ref']['id']
                        tupel.append(rule)
                        favorited_by_bots.append(tupel)

                csv_writer.writerow([
                    entry['tweet_id'], entry['user_ref']['screen_name'],
                    entry['text'], entry['url'], entry['media_url'],
                    entry['media_type'], entry['media_path'], retweeted_bot,
                    entry['in_reply_to_status_id'], replied_tweet['text'],
                    entry['retweet_count'], entry['favorite_count'],
                    entry['possibly_sensitive'], entry['place'],
                    retweeted_by_bots, favorited_by_bots, entry['created_at']
                ])

            except Exception as ex:
                pass


@logger
def export_rules(date):
    export_dir = get_export_dir(date)
    path = join(export_dir, 'rules.csv')
    if not exists(path):
        open(path, 'x').close()

    entry = {}
    entry['id'] = ""
    entry['trigger_ref'] = {}
    entry['trigger_ref']['trigger'] = ""
    entry['answer_ref'] = {}
    entry['answer_ref']['answer'] = ""

    with open(path, 'w') as csv_file:
        csv_writer = csv.writer(csv_file)
        csv_writer.writerow(['rule_id', 'trigger', 'answer'])

        dicts_json = get_all_rules()
        for entry in dicts_json["rows"]:
            csv_writer.writerow([
                entry['id'], entry['trigger_ref']['trigger'],
                entry['answer_ref']['answer']
            ])


def get_export_dir(date):
    parent_dir = 'facebookbot_data'
    home = Path.home()
    export_dir = join(home, parent_dir, 'export', date)
    if not exists(export_dir):
        makedirs(export_dir)

    return export_dir


def download_media(bot_name, tweet):
    """
    helper method to download the media of every tweet
    """
    try:
        url = tweet["entities"]["media"][0]["url"]
        mtype = tweet["entities"]["media"][0]["type"]
        path = download_media(url, mtype, bot_name)
        file_dir = path.split('/')[-1]
        if not exists(file_dir):
            makedirs(file_dir)
        log(__name__, 'Downloading media from tweet: {}'.format(tweet['id']))
    except KeyError:
        pass
    except Exception as ex:
        log(__name__, 'ERROR in download_media: {}'.format(ex))
    return path
