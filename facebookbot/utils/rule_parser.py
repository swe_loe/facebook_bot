from pyparsing import (Suppress, alphanums, Group, OneOrMore, ParseException,
                       Word, CaselessLiteral, Optional, Or)
from pprint import pprint
"""
grammar:
rule ::= if_smt tweet contains_smt trigger [and_smt trigger] [or_smt trigger] then_smt action [and_smt action][or_smt action] # noqa
if_smt ::= alpha+
tweet ::= alpha+
contains_smt ::= alpha+
trigger ::= alphanums+
then_smt ::= alpha+
action ::= alphanums+
or_smt ::= alpha+
and_smt ::= alpha+
"""


class BaseParser(object):
    """
    This class defines the base grammar for all parsers
    """

    def __init__(self):
        self.if_smt = Suppress(CaselessLiteral('if'))
        self.tweet = Suppress(CaselessLiteral('tweet'))
        self.contains_smt = Suppress(CaselessLiteral('contains'))
        self.trigger = Word(alphanums + 'üöäÄÜÖß,?!.')
        self.then_smt = Suppress(CaselessLiteral('then'))
        self.action = Word(alphanums + 'üöäÄÜÖß,?!.')
        self.quote = Suppress('"')
        self.or_smt = CaselessLiteral('or')
        self.and_smt = CaselessLiteral('and')
        self.l_paren = CaselessLiteral('(')
        self.r_paren = CaselessLiteral(')')


class SimpleParser(BaseParser):
    """
    Parser for simple ruleset
    """

    def __init__(self):
        super(SimpleParser, self).__init__()
        self.simple_triggers = self.quote + Group(OneOrMore(
            self.trigger))('triggers') + self.quote
        self.simple_actions = self.quote + Group(OneOrMore(
            self.action))('actions') + self.quote
        self.simple_rule = self.if_smt \
                           + self.tweet \
                           + self.contains_smt \
                           + self.simple_triggers \
                           + self.then_smt \
                           + self.simple_actions

    def parse(self, rule):
        """
        Parse rules of the format
        >>> if tweet contains "trigger" then "action"
        triggers and actions can be a single or multiple words but
        need to be sorounded by "", otherwise parsing fails
        """
        try:
            parse_result = self.simple_rule.parseString(rule)
            ret_val = {
                'triggers': ' '.join(list(parse_result['triggers'])),
                'actions': ' '.join(list(parse_result['actions']))
            }
            return ret_val
        except:
            raise ParseException(rule)


class ComplexParser(BaseParser):
    def __init__(self):
        super(ComplexParser, self).__init__()
        self.complex_triggers = self.quote + Group(OneOrMore(
            self.trigger))('trigger') + self.quote + Optional(
                OneOrMore(
                    Or([self.or_smt, self.and_smt]) + self.quote +
                    Group(OneOrMore(self.trigger)) + self.quote)('triggers2'))

        self.complex_actions = self.quote + Group(OneOrMore(
            self.action))('action') + self.quote + Optional(
                OneOrMore(
                    Or([self.or_smt, self.and_smt]) + self.quote +
                    Group(OneOrMore(self.action)) + self.quote)('actions2'))

        self.complex_rule = self.if_smt \
                           + self.tweet \
                           + self.contains_smt \
                           + self.complex_triggers \
                           + self.then_smt \
                           + self.complex_actions

    def parse(self, rule):
        try:
            parse_result = self.complex_rule.parseString(rule)
            trigger = parse_result['trigger']
            triggers2 = parse_result['triggers2']
            action = parse_result['action']
            actions2 = parse_result['actions2']

            ret_val = {
                'trigger': trigger,
                'triggers2': triggers2,
                'action': action,
                'actions2': actions2
            }
            pprint(ret_val)

        except Exception as e:
            print(e)
