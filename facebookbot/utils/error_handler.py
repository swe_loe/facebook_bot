class InvalidUsage(Exception):
    """
    Prepares a custom error message for http responses
    """
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        ret_val = dict(self.payload or ())
        ret_val['message'] = self.message
        return ret_val
