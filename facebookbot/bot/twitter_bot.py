#!/usr/bin/python
from threading import Thread, Event
from random import randrange

from time import sleep
from facebookbot.utils.logger import for_all_methods, logger, log
from facebookbot.utils.media_downloader import download_media
from facebookbot.utils import db_helpers as database
from multiprocessing import Process, Queue
from enum import Enum
import tweepy
import importlib


def get_user(tokens, user):
    """
    Separate method to have access to the bots information, before the actual database object creation.
    """
    try:
        auth = tweepy.OAuthHandler(tokens['consumer_key'],
                                   tokens['consumer_secret'])
        auth.set_access_token(tokens['access_token_key'],
                              tokens['access_token_secret'])
        api = tweepy.API(auth)
        return api.get_user(screen_name=user)._json
    except tweepy.TweepError as err:
        log(__name__, "ERROR : {}".format(str(err)))
        raise Exception("Invalid Tokens/Keys")
        return -1


def make_bot(tokens, bot_reference, bot_id):
    """
    Returns a user bot.
    bot_reference must be [<module>,<class>]
    """
    custom_bot = getattr(
        importlib.import_module(bot_reference[0]), bot_reference[1])
    return custom_bot(tokens['access_token_key'],
                      tokens['access_token_secret'], tokens['consumer_key'],
                      tokens['consumer_secret'], bot_id)


class QUEUE_CALLS(Enum):
    """
    Calls that can be exchanged between the processes
    """
    RATELIMIT = 0
    RESTART = 1
    TEST = 2


class STREAM_TYPE(Enum):
    """
    Different kinds of queue threads
    """
    STREAM = 0
    RESPONSE_STREAM = 1


class REACTION_TYPE(Enum):
    """
    Different kinds of fetch user reaction threads
    """
    RETWEETED = 0
    FOLLOWER = 1
    MESSAGE = 2


@for_all_methods(logger)
class TwitterBot(Thread):
    """
    This is the bot class.
    """

    def __init__(self, access_token_key, access_token_secret, consumer_key,
                 consumer_secret, bot_id):
        super(TwitterBot, self).__init__()
        self.access_token_key = access_token_key
        self.access_token_secret = access_token_secret
        self.consumer_key = consumer_key
        self.consumer_secret = consumer_secret
        self.bot_id = bot_id
        model = database.get_bot(bot_id)
        self.bot_name = model['bot_name']
        """
        Minimal time between original post and the bot's reply in seconds
        """
        self.reply_min_delay = model['min_resp_t']
        """
        Maximal time between original post and the bot's reply in seconds
        """
        self.reply_max_delay = model['max_resp_t']
        """
        skip relevant tweets (we don't want to reply to EVERY tweet)
        """
        self.skip_tweets = 0
        """

        Interval time between each call of "action", in seconds
        """
        self.routine_time = model['routine_t']
        """
        Only reply to friends (using the streamlistener)
        """
        self.reply_only_friends = False
        """
        Stops the main routine.
        """
        self.stop_mainloop = False
        """
        Stop the stream listener
        """
        self.stop_stream = False
        """
        Stop the thread for fetching other reactions to us
        """
        self.stop_fetching_reactions = False
        """
        Stop the response_stream
        """
        self.stop_response_stream = False
        """
        Stop the bot instance
        """
        self.STOP = False
        """
        Cycle time for fetching the retweets
        """
        self.retweeted_listener_sleep = model['retweeted_sleep']
        """
        Cycle time for fetching new followers
        """
        self.followers_listener_sleep = model['follower_sleep']
        """
        Cycle time for fetching new direct messages
        """
        self.direct_message_listener_sleep = model['d_message_sleep']
        """
        set rules
        """
        self.rules = database.get_rules_from_db(bot_id)
        """
        Cycle time for writing stats to the database
        """
        self.statistics_schedule_time = 60 * 5

        try:
            self.auth = tweepy.OAuthHandler(self.consumer_key,
                                            self.consumer_secret)
            self.auth.set_access_token(self.access_token_key,
                                       self.access_token_secret)

            self.api_obj = tweepy.API(self.auth)
        except tweepy.TweepError as err:
            log(__name__, "ERROR : {}".format(str(err)))
            raise Exception("Invalid Tokens/Keys")

        self.twitter_id = self.api_obj.me().id
        self.screen_name = self.api_obj.me().screen_name
        """
        Cycle time for the queue threads to listen for new messages
        """
        self.queue_sleep = 10

        self.WAIT_RATELIMIT = 901
        """
        init save statistics 
        """
        database.save_statistics(bot_id, self.api_obj.me()._json)

    """
    THREAD METHODS
    """

    def run(self):
        """
        Starts the bot
        start mainloop & listener
        """
        self.STOP = False
        self.mainloop()
        self.start_processes()
        self.start_fetch_user_reactions()
        self.start_statistics_schedule()

    """
    METHODS TO START LISTENERS / ROUTINES
    """

    def mainloop(self):
        """
        Start the main routine.
        """
        self.stop_mainloop = False
        self.mainloop_thread = Thread(target=self.__mainloop_thread)
        self.mainloop_thread.start()

    def start_fetch_user_reactions(self):
        """
        Start the fetch_user_reactions threads
        """
        self.stop_fetching_reactions = False
        self.follower_listener = Thread(
            target=self.fetch_user_reactions,
            args=(REACTION_TYPE.FOLLOWER, self.followers_listener_sleep))
        self.retweeted_listener = Thread(
            target=self.fetch_user_reactions,
            args=(REACTION_TYPE.RETWEETED, self.retweeted_listener_sleep))
        #self.direct_message_listener = Thread(
        #    target=self.fetch_user_reactions,
        #    args=(REACTION_TYPE.MESSAGE, self.direct_message_listener_sleep))

        self.follower_listener.start()
        self.retweeted_listener.start()
        #self.direct_message_listener.start()

    def start_processes(self):

        self.stop_stream = False
        self.stop_response_stream = False

        self.queue_response_stream = Queue()

        self.process_response_stream = Process(
            target=start_process_response_stream,
            args=(self.queue_response_stream, self))

        self.queue_response_stream_runner = Thread(
            target=self.queue_stream_thread,
            args=(self.queue_response_stream, STREAM_TYPE.RESPONSE_STREAM))

        self.process_response_stream.start()
        self.queue_response_stream_runner.start()

        if self.rules:
            #only if rules exist, init and start the streamer
            self.queue_stream = Queue()
            self.process_stream = Process(
                target=start_process_stream, args=(self.queue_stream, self))
            self.queue_stream_runner = Thread(
                target=self.queue_stream_thread,
                args=(self.queue_stream, STREAM_TYPE.STREAM))

            self.process_stream.start()
            self.queue_stream_runner.start()

    def start_statistics_schedule(self):
        self.statistics_schedule_thread = Thread(
            target=self.__statistics_schedule).start()

    """
    METHODS TO STOP LISTENERS
    """

    def stop(self):
        """
        stops the bot instance
        """
        self.STOP = True
        self.stop_processes()
        #save stats before terminating
        database.save_statistics(self.bot_id, self.api_obj.me()._json)

    def stop_mainloop_thread(self):
        """
        Stop the main routine.
        """
        self.stop_mainloop = True

    def stop_fetch_user_reaction(self):
        """
        Stop every other listener (retweets,followers,direct messages)
        """
        self.stop_fetching_user_reactions = True

    def stop_processes(self):
        """
        Stop the twitter stream processes, and queue listeners
        """
        try:
            self.process_response_stream.terminate()
            self.process_stream.terminate()
            self.stop_stream = True
        except Exception:
            log(__name__, "Process(es) already terminated")

    """
    UTILITY / OTHER METHODS
    """

    def set_response_time(self, reply_min_delay, reply_max_delay,
                          routine_time):
        """
        Set response time.
        """
        if (reply_min_delay >= reply_max_delay or reply_min_delay <= 0
                or reply_max_delay <= 0):
            raise ValueError(
                "Wrong values for reply_min_delay, reply_max_delay")
        if routine_time <= 0:
            raise ValueError("Wrong value for routine_time")

        self.reply_min_delay = reply_min_delay
        self.reply_max_delay = reply_max_delay
        self.routine_time = routine_time

    def set_listener_time(self, args):
        """
        Set the sleep times for the fetch_user_activity threads
        """
        if len(args) != 3 and not all(i > 0 for i in args):
            raise ValueError(
                "Wrong values or number of arguments for set_listener_time")
        self.retweeted_listener_sleep = args[0]
        self.followers_listener_sleep = args[1]
        self.direct_message_listener_sleep = args[2]

    def set_skip_tweets(self, skip_tweets):
        """
        Set how many relevant tweets should be skipped
        """
        if skip_tweets < 0:
            raise ValueError("Wrong value for skip_tweets")
        self.skip_tweets = skip_tweets

    def get_bot_data(self):
        """
        returns all relevant data for the bot account itself.
        data = bot.get_bot_data()
        relevant entries:
            data.id                 twitter-id
            data.name               twitter-name
            data.screen_name        @name
            data.description
            data.statuses_count
            data.status             last status
            data.status.text        text of last status
            data.followers_count    number of followers
            data.friends_count      number of accounts the bot is following
            data.created_at         bot's birthday
            data.favourites_count
            data.profile_image      url of profile image
        """
        return self.api_obj.me()

    def ratelimit(self):
        """
        Shortcut to get the ratelimit status
        """
        return self.api_obj.rate_limit_status()

    def __mainloop_thread(self):
        """
        Thread for the main routine
        """
        while True:
            if self.stop_mainloop or self.STOP:
                return
            sleep(self.routine_time)
            try:
                self.action()
            except tweepy.RateLimitError:
                self.ratelimit_alert()
                sleep(self.WAIT_RATELIMIT)
            except Exception as err:
                log(__name__, "ERROR IN mainloop: " + str(err))
                raise

    def __statistics_schedule(self):
        """
        Regularly writing the User Object into the db
        """
        while not self.STOP:
            database.save_statistics(self.bot_id, self.api_obj.me()._json)
            sleep(self.statistics_schedule_time)

    def set_rules(self, rules):
        """
        Set the rules for the reply behavior.
        """
        if rules is not None and isinstance(rules, dict):
            self.rules = rules
        else:
            raise Exception("set_rules: invalid argument")

    def fetch_user_reactions(self, name, sleep_time):
        """
        Who followed us, retweeted us, messaged us?
        Runs in a thread
        olist = list of retweets or followers
        name  = self.__RETWEETED_NAME | self.__FOLLOWERS_NAME |
        .self.__DIRECT_MESSAGE_NAME
        sleep_time = self.retweeted_listener_sleep
        | self.followers_listener_sleep | self.direct_message_listener_sleep
        """
        while not self.stop_fetching_reactions and not self.STOP:
            """
            Go backwards through the list of recent <objects>.
            Skip until we find our latest known <object>
            Add every <object> after to the database
            Add timestamp from the moment we recoginze the
            creation time of the <object>
            """
            sleep(sleep_time)
            id_latest = None
            try:
                if name == REACTION_TYPE.FOLLOWER:
                    id_latest = database.get_latest_follower(self.bot_id)
                    olist = self.api_obj.followers()
                elif name == REACTION_TYPE.RETWEETED:
                    id_latest = database.get_latest_retweeted(self.bot_id)
                    olist = self.get_retweets()
                elif name == REACTION_TYPE.MESSAGE:
                    #id_latest = database.get_latest_direct_message(self.bot_id)
                    olist = self.get_messages()
            except tweepy.RateLimitError:
                # to be tested...
                self.ratelimit_alert()
                sleep(self.WAIT_RATELIMIT)
                continue
            except Exception as ex:
                #something else went wrong
                log(__name__, 'ERROR in fetch_user_activity: {}'.format(ex))
                continue
            counter = 1
            skip = True
            if len(olist) > 0:
                if id_latest is None:
                    id_latest = olist[-1].id
                while True:
                    if olist[-counter].id != id_latest and skip:
                        if counter == len(olist):
                            break
                        else:
                            counter += 1
                    else:
                        skip = False
                        if counter == len(olist):
                            # if we reached the end, break, sleep and start over again
                            id_latest = olist[-counter].id
                            break
                        else:
                            # else go to the first new entry which we don't know
                            counter += 1
                        if name == REACTION_TYPE.FOLLOWER:
                            database.save_follower(self.bot_id,
                                                   olist[-counter]._json)
                            log(__name__, 'New Follower: {}'.format(
                                olist[-counter].id))
                        elif name == REACTION_TYPE.RETWEETED:
                            database.save_retweeted(self.bot_id,
                                                    olist[-counter]._json)
                            log(__name__, 'Tweet got retweeted: {}'.format(
                                olist[-counter].id))
                        elif name == REACTION_TYPE.MESSAGE:
                            #database.add_direct_message(olist[-counter]._json)
                            log(__name__, 'New direct message: {}'.format(
                                olist[-counter].id))

    def queue_stream_thread(self, queue, stream):
        while not self.stop_stream and not self.STOP:
            if not queue.empty():
                message = queue.get()
                if isinstance(message, QUEUE_CALLS):
                    if message == QUEUE_CALLS.RATELIMIT:
                        self.ratelimit_alert()
                    if message == QUEUE_CALLS.RESTART:
                        self.start_processes()
                        break
                    if message == QUEUE_CALLS.TEST:
                        print("process is running")
                else:
                    status = message
                    try:
                        if stream == STREAM_TYPE.STREAM:
                            for rule in self.rules:
                                if rule in status.text:
                                    self.reply(self.rules[rule], status, rule)
                        if stream == STREAM_TYPE.RESPONSE_STREAM:
                            self.tweet_reaction(status)
                    except tweepy.TweepError as err:
                        if err.api_code == 187:
                            raise Exception("Status ist ein Duplikat")
                    except tweepy.RateLimitError:
                        ratelimit_alert()
                    except Exception as ex:
                        log(__name__,
                            'ERROR in queue_stream_thread: {}'.format(ex))
            sleep(self.queue_sleep)

    def tweet_reaction(self, tweet):
        """
        Gets called when a tweet from
        someone mentions the bot/replies to the bot
        """
        try:
            database.save_mentions(self.bot_id, tweet._json)
            log(__name__, 'Tweet mentioned us: {}'.format(tweet.id))
        except Exception as ex:
            log(__name__, 'ERROR in tweet_reaction: {}'.format(ex))

    def ratelimit_alert(self):
        """
        handle ratelimits
        """
        log(__name__, 'Ratelimit hit')
        raise Exception(
            "Twitters Ratelimit wurde erreicht. Botaktivitäten werden nun für 15 Minuten unterbrochen."
        )

    """
    USER BOT METHODS
    """

    def get_my_tweets(self):
        """
        User bot method.
        return my (20 latest) tweets
        """
        try:
            return self.api_obj.user_timeline(self.twitter_id)
        except:
            return None

    def tweet(self, message):
        """
        User bot method.
        Post a tweet.
        Returns status object
        """
        try:
            new_status = self.api_obj.update_status(message)
            database.save_tweet(self.bot_id, new_status._json)
            log(__name__, 'New tweet send: {}'.format(message))
            return new_status
        except tweepy.RateLimitError:
            ratelimit_alert()
        except tweepy.TweepError as err:
            if err.api_code == 187:
                raise Exception("Status ist ein Duplikat")

    def remove_tweet(self, tweet_id):
        """
        User bot method.
        Delete tweet.
        Return object
        """
        removed = self.api_obj.destroy_status(tweet_id)
        if database.delete_tweet(tweet_id) != -1:
            log(__name__, 'Tweet removed : {}'.format(tweet_id))
        return removed

    def reply(self, message, tweet, trigger=''):
        """
        Reply to a tweet with message
        Save the tweet and the reply to the database
        Returns status object
        """
        log(__name__, 'Tweet was filtered: {}'.format(tweet.id))
        new_status = None
        if tweet.user.id != self.twitter_id:
            try:
                reply_names = "@" + tweet.retweeted_status.user.screen_name\
                              + " @" + tweet.user.screen_name
            except AttributeError:
                reply_names = "@" + tweet.user.screen_name
            finally:
                new_status = self.api_obj.update_status(
                    reply_names + " " + message,
                    in_reply_to_status_id=tweet.id)
            database.save_reply(self.bot_id, new_status._json, trigger)
            database.save_triggertweet(self.bot_id, tweet._json, new_status.id)
        log(__name__,
            'New reply with message : {} , tweet text: {} , trigger: {}'.
            format(message, tweet.text, trigger))
        return new_status

    def get_tweets(self):
        """
        User bot method.
        Get the bots timeline, and remove all tweets which the bot made itself
        Returns list of status-objects
        """
        timeline = self.api_obj.home_timeline()
        return [
            tweet for tweet in timeline if tweet.user.id != self.twitter_id
        ]

    def get_tweet_message(self, tweet):
        """
        User bot method.
        Get the message from a tweet object
        if something goes wrong return None
        """
        try:
            return tweet.text
        except:
            return None

    def get_tweet_user(self, tweet):
        """
        User bot method
        Returns all mentioned users in a tweet as a list
        Usage get_tweet_user(tweet)[0]['screen_name']
        """
        try:
            return tweet.entities['user_mentions']
        except:
            return None

    def get_retweets(self):
        """
        User bot method.
        Get my tweets, that have been retweeted by others
        Returns list of status-objects
        if something goes wrong return None
        """
        try:
            return self.api_obj.retweets_of_me()
        except:
            return None

    def favorite(self, tweet):
        """
        User bot method.
        Favorite a tweet. if already favorited, unfavorite
        Returns status object
        """
        try:
            for fav in self.api_obj.favorites(self.twitter_id):
                if fav.id == tweet.id:
                    new_fav = self.api_obj.destroy_favorite(tweet.id)
                    database.delete_favorite(tweet._json)
                    log(__name__, 'Favorite removed: {}'.format(tweet.id))
                    return new_fav

                else:
                    # download media of tweets we favorite
                    new_fav = self.api_obj.create_favorite(tweet.id)
                    database.save_favorite(
                        bot_id, tweet._json, ''
                    )  ##nothing for rule_key, because we can't have rules for favorites
                    log(__name__, 'Favorite added: {}'.format(tweet.id))
                    return new_fav
        except tweepy.TweepError as err:
            # if no tweets have been favorited yet
            if err.api_code == 34:
                new_fav = self.api_obj.create_favorite(tweet.id)
                database.save_favorite(tweet._json)
                return new_fav
            else:
                log(__name__, 'ERROR in favorite: {}'.format(err))

    def follow(self, user):
        """
        User bot method.
        Follow user. if already following, unfollow
        Returns user object
        """
        if not self.api_obj.show_friendship(
                source_id=self.twitter_id, target_id=user.id)[0].following:
            friendship = self.api_obj.create_friendship(user.id)
            database.save_friend(self.bot_id, user._json)
            log(__name__, 'Friend added: {}'.format(user.screen_name))
            return friendship
        else:
            database.delete_friend(user._json)
            log(__name__, 'Friend removed: {}'.format(user.screen_name))
            return self.api_obj.destroy_friendship(user.id)

    def check_following(self, user):
        """
        User bot method.
        Check if the bot follows user
        user can be the name (string) or the user object
        if something goes wrong return None
        """
        if isinstance(user, str):
            name = user
        else:
            try:
                name = user.screen_name
            except:
                log(__name__, 'ERROR in check_following')
                return None
        return self.api_obj.show_friendship(
            source_screen_name=database.get_bot(
                self.bot_id)['user_ref']['screen_name'],
            target_screen_name=name)[0].following

    def retweet(self, tweet):
        """
        User bot method.
        retweet a tweet. if already retweeted, undo retweet
        Returns status object
        """
        retweet = None
        try:
            retweet = self.api_obj.retweet(tweet.id)
        except tweepy.TweepError as err:
            if err.api_code == 327:
                # already retweeted, so undo retweet:
                # We cannot get easy access to our retweets,
                # so we save our retweets in the database
                #
                retweet = database.get_retweet(self.bot_id, tweet.id)
                # status.retweeted_status = our status object from the original status # noqa
                self.api_obj.destroy_status(retweet.retweeted_status.id)
                database.delete_retweet(tweet.id)
                log(__name__, 'Undid Retweet : {}'.format(tweet.id))
                pass
            else:
                # Raise the error if it is not 327
                raise
        # download media of retweets
        database.save_retweet(
            self.bot_id, tweet.id, ''
        )  #nothing for rule_key, because we can't have rules for retweets
        log(__name__, 'Retweeted: {}'.format(tweet.id))
        return retweet

    def get_user(self, screen_name):
        """
        Get user by screen_name
        return None, if user does not exist
        """
        user = None
        if not screen_name:
            return "Name is emtpy!"
        try:
            user = self.api_obj.get_user(screen_name=screen_name)
        except tweepy.TweepError as err:
            if err.api_code == 50:
                log(__name__, 'User {} not found'.format(screen_name))
                raise Exception(
                    "No user with name {} found".format(screen_name))
        return user

    def action(self):
        """
        User bot method.
        This method is performed everytime after routine_time has passed
        This method should be implemented in a subclass
        """
        pass

    def get_messages(self):
        """
        User bot method.
        Returns the direct messages to the bot
        """
        return self.api_obj.direct_messages()

    def send_message(self, user_id, message):
        """
        User bot method
        Send a direct message to user_id
        """
        message_obj = self.api_obj.send_direct_message(
            user_id=user_id, text=message)
        # self.database.add_sent_message(self.bot_id,user_id,message
        log(__name__, 'Send message: {} , To: {}'.format(message, user_id))
        return message_obj

    def get_friends(self):
        return self.api_obj.friends()

    def get_followers(self, id=None):
        return self.api_obj.followers(id)

    def get_last_tweet(self, user=None, index=0):
        account_id = 0
        if user is None:
            account_id = self.twitter_id
        else:
            account_id = user.id
        tweet = self.api_obj.user_timeline(id=account_id)[index]
        return tweet


def start_process_stream(queue, twitter_bot):
    """
    Entry point for the stream listener process
    Connects to stream.twitter and listens for relevant tweets
    """
    twitter_bot.stream_listener = TwitterBotStreamListener()
    twitter_bot.stream_listener.init(twitter_bot, queue)
    twitter_bot.stream = tweepy.Stream(
        auth=twitter_bot.auth, listener=twitter_bot.stream_listener)
    twitter_bot.stream.filter(track=twitter_bot.rules.keys())


def start_process_response_stream(queue, twitter_bot):
    """
    Entry point for the response stream listener process
    Connects to stream.twitter and listens for mentions/replies
    """
    twitter_bot.response_stream_listener = TwitterBotResponseListener()
    twitter_bot.response_stream_listener.init(twitter_bot, queue)
    twitter_bot.response_stream = tweepy.Stream(
        auth=twitter_bot.auth, listener=twitter_bot.response_stream_listener)

    twitter_bot.response_stream.filter(track=[twitter_bot.screen_name])


class TwitterBotSuperStreamer(tweepy.StreamListener):
    """
    Superclass to implement on_error behavoir in case of ratelimit
    """

    def init(self, twitter_bot, queue):
        self.twitter_bot = twitter_bot
        self.queue = queue
        self.event = Event()
        self.event.set()


@for_all_methods(logger)
class TwitterBotStreamListener(TwitterBotSuperStreamer):
    """
    The StreamListener. Fetches tweets at real time from twitter.
    """

    def init(self, twitter_bot, queue):
        super(TwitterBotStreamListener, self).init(twitter_bot, queue)
        self.counter_skip_tweets = 0
        self.queue.put(QUEUE_CALLS.TEST)

    def wait_reply(self, status, sleep_time):
        """
        Wait for some time before replying.
        """
        sleep(sleep_time)
        self.queue.put(status)
        self.event.set()  # set flag to true, waking up thread

    def on_status(self, status):
        """
        Gets triggered, everytime one tweet matches the filter
        """
        try:
            # block until flag is true
            self.event.wait()
            if self.counter_skip_tweets == self.twitter_bot.skip_tweets:
                if (not self.twitter_bot.reply_only_friends) ^ (
                        self.twitter_bot.reply_only_friends
                        and self.twitter_bot.api_obj.show_friendship(
                            source_id=self.twitter_bot.twitter_id,
                            target_id=status.user.id)[0].following):
                    # flag is true here, so set it to false
                    self.event.clear()
                    Thread(
                        target=self.wait_reply,
                        args=(status,
                              randrange(self.twitter_bot.reply_min_delay,
                                        self.twitter_bot.reply_max_delay,
                                        1))).start()
                self.counter_skip_tweets = 0
            else:
                self.counter_skip_tweets += 1
        except tweepy.RateLimitError:
            self.queue.put(QUEUE_CALLS.RATELIMIT)
            sleep(self.twitter_bot.WAIT_RATELIMIT)
            self.queue.put(QUEUE_CALLS.RESTART)
            return False

        except Exception as err:
            log(__name__, "ERROR IN Stream Listener: " + str(err))
            raise


@for_all_methods(logger)
class TwitterBotResponseListener(TwitterBotSuperStreamer):
    """
    Streamlistener, which handles everything,
    that comes as a reply or mention to our bot.
    """

    def on_status(self, status):
        try:
            self.queue.put(status)
        except tweepy.RateLimitError:
            self.queue.put(QUEUE_CALLS.RATELIMIT)
            sleep(self.twitter_bot.WAIT_RATELIMIT)
            self.queue.put(QUEUE_CALLS.RESTART)
            return False
        except Exception as err:
            log(__name__, "ERROR IN Response Listener: " + str(err))
            raise
