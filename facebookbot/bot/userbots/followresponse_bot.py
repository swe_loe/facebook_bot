#!/usr/bin/python
from facebookbot.bot.twitter_bot import TwitterBot


class FollowResponseBot(TwitterBot):
    """
    Bot behaviour
    """

    def action(self):
        """
        Override the function
        """
        tweets = self.get_tweets()
        for tweet in tweets:
            users = self.get_tweet_user(tweet)
            for user in users:
                user = self.get_user(user['screen_name'])
                if not self.check_following(user):
                    self.follow(user)
                    self.tweet("I am following now:  @" + user.screen_name)
                    break

    def tweet_reaction(self, tweet):
        """
        Override the function.
        This gets called when a tweet contains our name
        'tweet' is the corresponding status
        """
        super(FollowResponseBot, self).tweet_reaction(tweet)
        self.reply("you too", tweet)
