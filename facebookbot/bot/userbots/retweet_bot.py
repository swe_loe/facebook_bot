from facebookbot.bot.twitter_bot import TwitterBot


class RetweetBot(TwitterBot):
    """
    Bot behaviour
    """

    def action(self):
        """
        Override the function
        """
        tweets = self.get_tweets()
        for tweet in tweets:
            if 'Retweet if you agree' in self.get_tweet_message(tweet):
                self.retweet(tweet)

    def tweet_reaction(self, tweet):
        """
        Override the function.
        This gets called when a tweet contains our name
        'tweet' is the corresponding status
        """
        super(RetweetBot, self).tweet_reaction(tweet)
