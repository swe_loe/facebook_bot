from facebookbot.bot.twitter_bot import TwitterBot


class AggroBot(TwitterBot):
    """
    Bot behaviour
    """

    def action(self):
        """
        Override the function
        """
        print("My custom action!")

    def tweet_reaction(self, tweet):
        """
        Override the function.
        This gets called when a tweet contains our name
        'tweet' is the corresponding status
        """
        super(AggroBot, self).tweet_reaction(tweet)
