import os
from pathlib import Path
from os.path import join, exists, dirname, abspath

BASE_DIR = abspath(dirname(abspath(dirname(__file__))))


class ProdConfig(object):
    """
    This configuration is used for production
    """
    parent_dir = 'facebookbot_data'
    home = Path.home()
    DB_DIR = join(str(home), parent_dir, 'database')
    if not exists(DB_DIR):
        os.makedirs(DB_DIR)
    DATABASE = ''.join(['sqlite:///', DB_DIR, '/prod.db'])  # noqa E501
    DEBUG = False


class TestConfig(object):
    """
    This configuration is used for local manual testing
    """
    DATABASE = ''.join(['sqlite:///', BASE_DIR, '/test.db'])  # noqa E501
    DEBUG = True


class UnitTestConfig(object):
    """
    This configuration is used for automated unittests
    """
    DATABASE = ''.join(['sqlite:///', BASE_DIR,
                        '/tests/unittest.db'])  # noqa E501
    DEBUG = True
