from random import SystemRandom
from time import time
from datetime import datetime
from peewee import (
    CharField,
    DateTimeField,
    BooleanField,
    ForeignKeyField,
)
from peewee import IntegerField
from facebookbot import db_wrapper


def create_bot_hash(bot_name):
    """creates a random bot id, if random id is already in
    use, a other id is generated"""
    bots = TwitterBotModel.select()
    # not pretty but works
    used_bot_hashes = [bot.bot_hash for bot in bots]
    while True:
        random_factor = SystemRandom(time()).random()
        bot_hash = str(hash(bot_name + str(random_factor)))[1:13]
        if bot_hash not in used_bot_hashes:
            break
    return bot_hash


# if no custom primary key is specified peewee creates its own primary keys
# which are auto-incremented


class BotType(db_wrapper.Model):
    # Bottypes used to create the needed classes
    filename = CharField(null=False)
    import_name = CharField(null=False)
    bot_class = CharField(null=False)
    created = DateTimeField(null=False, default=datetime.now)


class DefaultSetting(db_wrapper.Model):

    min_resp_t = IntegerField(default=2000)
    max_resp_t = IntegerField(default=5000)
    routine_t = IntegerField(default=3000)
    retweeted_sleep = IntegerField(default=1000)
    follower_sleep = IntegerField(default=1000)
    d_message_sleep = IntegerField(default=1000)


class User(db_wrapper.Model):
    # Informations about a Twitteruser
    user_id = IntegerField(primary_key=True, unique=True)
    screen_name = CharField(null=True)
    location = CharField(null=True)
    followers_count = IntegerField(null=True)
    friends_count = IntegerField(null=True)
    created_at = DateTimeField(null=True)
    favourites_count = IntegerField(null=True)
    verified = BooleanField(null=True)
    statuses_count = IntegerField(null=True)
    following = IntegerField(null=True)


class TwitterBotModel(db_wrapper.Model):
    # Our Twitterbots
    bot_name = CharField(null=False)
    type_ref = ForeignKeyField(BotType)
    user_ref = ForeignKeyField(User)
    # settings_ref = ForeignKeyField(DefaultSetting)
    bot_hash = CharField(unique=True)
    user_name = CharField(null=False)
    access_token_key = CharField(null=False)
    access_token_secret = CharField(null=False)
    consumer_key = CharField(null=False)
    consumer_secret = CharField(null=False)
    visible = BooleanField(default=1)
    min_resp_t = IntegerField(null=True)
    max_resp_t = IntegerField(null=True)
    routine_t = IntegerField(null=True)
    retweeted_sleep = IntegerField(null=True)
    follower_sleep = IntegerField(null=True)
    d_message_sleep = IntegerField(null=True)
    created = DateTimeField(default=datetime.now)


class Tweet(db_wrapper.Model):
    # Informations about a Tweet
    tweet_id = IntegerField(primary_key=True, unique=True)
    user_ref = ForeignKeyField(User)
    created_at = DateTimeField(null=True)
    text = CharField(null=True)
    url = CharField(null=True)
    media_url = CharField(null=True)
    media_type = CharField(null=True)
    in_reply_to_status_id = CharField(null=True)
    in_reply_to_status_id_str = CharField(null=True)
    place = CharField(null=True)
    retweet_count = IntegerField(null=True)
    favorite_count = IntegerField(null=True)
    favorited = IntegerField(null=True)
    retweeted = IntegerField(null=True)
    media_path = CharField(null=True)
    possibly_sensitive = BooleanField(null=True)


class Trigger(db_wrapper.Model):
    # These are the words if found in a post the bot would react to them
    trigger = CharField(null=False)
    created = DateTimeField(null=False, default=datetime.now)
    enabled = BooleanField(default=1)


class Answer(db_wrapper.Model):
    # The predefined answers to a certain Trigger
    answer = CharField(null=False)
    created = DateTimeField(null=False, default=datetime.now)
    enabled = BooleanField(default=1)


class Rule(db_wrapper.Model):
    # Joins Trigger and Answer
    trigger_ref = ForeignKeyField(Trigger)
    answer_ref = ForeignKeyField(Answer)


class Botrule(db_wrapper.Model):
    # Joins Bot and Rule
    bot_ref = ForeignKeyField(TwitterBotModel)
    rule_ref = ForeignKeyField(Rule)


class Retweet(db_wrapper.Model):
    # We retweeted
    tweet_ref = ForeignKeyField(Tweet)
    rule_ref = ForeignKeyField(Rule)
    retweeted_status_ref = ForeignKeyField(Tweet)
    created = DateTimeField(null=False, default=datetime.now)


class Reply(db_wrapper.Model):
    # We reply to something
    tweet_ref = ForeignKeyField(Tweet)
    rule_ref = ForeignKeyField(Rule)
    replied_status_ref = ForeignKeyField(Tweet)
    created = DateTimeField(null=False, default=datetime.now)


class Favorite(db_wrapper.Model):
    # We favorite something
    tweet_ref = ForeignKeyField(Tweet)
    rule_ref = ForeignKeyField(Rule)
    bot_ref = ForeignKeyField(TwitterBotModel)
    created = DateTimeField(null=False, default=datetime.now)


class Mention(db_wrapper.Model):
    tweet_ref = ForeignKeyField(Tweet)
    bot_ref = ForeignKeyField(TwitterBotModel)
    created = DateTimeField(null=False, default=datetime.now)


class Retweeted(db_wrapper.Model):
    # We were retweeted
    tweet_ref = ForeignKeyField(Tweet)
    bot_ref = ForeignKeyField(TwitterBotModel)
    created = DateTimeField(null=False, default=datetime.now)


# not used
class Favorited(db_wrapper.Model):
    # We were favorited
    tweet_ref = ForeignKeyField(Tweet)
    bot_ref = ForeignKeyField(TwitterBotModel)
    created = DateTimeField(null=False, default=datetime.now)


class Friend(db_wrapper.Model):
    # The people we follow
    user_ref = ForeignKeyField(User)
    bot_ref = ForeignKeyField(TwitterBotModel)
    created = DateTimeField(null=False, default=datetime.now)


class Follower(db_wrapper.Model):
    # The people who follow us
    user_ref = ForeignKeyField(User)
    bot_ref = ForeignKeyField(TwitterBotModel)
    created = DateTimeField(null=False, default=datetime.now)


class Triggertweet(db_wrapper.Model):
    # Tweet that triggered the bot
    tweet_ref = ForeignKeyField(Tweet)
    reply_ref = ForeignKeyField(Reply)
    created = DateTimeField(null=False, default=datetime.now)


class StatisticsLog(db_wrapper.Model):
    # Used to build a journal of bot data
    bot_ref = ForeignKeyField(TwitterBotModel)
    followers = IntegerField(null=False)
    follows = IntegerField(null=False)
    tweets = IntegerField(null=False)
    retweets = IntegerField(null=False)
    retweeted = IntegerField(null=False)
    likes = IntegerField(null=False)
    liked = IntegerField(null=False)
