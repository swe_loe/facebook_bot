import os
import sys
from flask import Flask
from playhouse.flask_utils import FlaskDB
from facebookbot.config import TestConfig, UnitTestConfig

db_wrapper = FlaskDB()


def create_app(test_config=None):
    """
    create a instance of the facebookbot flask app
    """
    app = Flask(__name__, instance_path=os.path.dirname(__file__))
    if test_config is TestConfig:
        app.config.from_object(test_config)
    elif test_config is UnitTestConfig:
        app.config.from_object(test_config)
    else:
        from .config import ProdConfig
        app.config.from_object(ProdConfig)
    # initialize database on app creation
    db_wrapper.init_app(app)
    from .models import (TwitterBotModel, Trigger, Answer, StatisticsLog,
                         BotType, Rule, Botrule, Follower, User, Tweet,
                         Retweeted, Favorite, Reply, Retweet, Triggertweet,
                         DefaultSetting, Friend, Favorited, Mention)
    from facebookbot.utils.db_helpers import (
        insert_bottype, save_defaultsettings, get_rule_id, save_rule_to_db)
    db_wrapper.database.connect()
    db_wrapper.database.create_tables([
        BotType, TwitterBotModel, Trigger, Answer, StatisticsLog, Rule,
        Botrule, Follower, User, Tweet, Retweeted, Favorite, Reply, Retweet,
        Triggertweet, DefaultSetting, Friend, Favorited, Mention
    ])

    insert_bottype()
    save_defaultsettings()
    rule_dict = {
        'actions': 'replydummy',
        'triggers': 'replydummy'
    }  # dummyrules for replies
    # FIXME ensure that dummy bot_id is never used
    save_rule_to_db(sys.maxsize, rule_dict)
    db_wrapper.database.close()

    from .views import bot
    app.register_blueprint(bot)
    return app
