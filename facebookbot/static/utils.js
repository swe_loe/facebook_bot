$(document).ready(function() {
    // this script is for saving settings and bots
    // also it is used to trigger the export of the database

    message_box = $('.hidden_messagebox');
    content_top = $('#content_top');
    function display_messagebox(message, successful) {
        // display stylish messagebox here
        $('.message').text(message);
        if (successful == true) {
            content_top.append(message_box.clone().removeClass('hidden hidden_messagebox').addClass('alert-success displayed_messagebox'));
        } else {
            content_top.append(message_box.clone().removeClass('hidden hidden_messagebox').addClass('alert-danger displayed_messagebox'));
        }
        $('.displayed_messagebox').fadeOut(6000, function() {
            content_top.empty();
        });

    }

    $('#save_settings').on('click', function(event) {
        min_resp_t = $('#min_resp_t').val();
        max_resp_t = $('#max_resp_t').val();
        routine_t = $('#routine_t').val();
        retweeted_sleep = $('#retweeted_sleep').val();
        follower_sleep = $('#follower_sleep').val();
        d_message_sleep = $('#d_message_sleep').val();

        if (min_resp_t && max_resp_t && routine_t && retweeted_sleep && follower_sleep && d_message_sleep) {
            if (min_resp_t > max_resp_t) {
                display_messagebox('Maximum response time must be greater than minimum response time', false);
            } else {
                data = {
                    'min_resp_t': parseInt(min_resp_t),
                    'max_resp_t': parseInt(max_resp_t),
                    'routine_t': parseInt(routine_t),
                    'retweeted_sleep': parseInt(retweeted_sleep),
                    'follower_sleep': parseInt(follower_sleep),
                    'd_message_sleep': parseInt(d_message_sleep),
                };
                if(bot_id){
                    data.bot_id = bot_id;
                }
                $.ajax({
                    type: "POST",
                    contentType: "application/json",
                    url: "/ch_bot_settings/",
                    data: JSON.stringify(data),
                    dataType: 'json',
                    success: function(_, _, result) {
                        json_resp = result.responseJSON;
                        display_messagebox(json_resp.message, true);
                    },
                    error: function(result) {
                        json_resp = result.responseJSON;
                        display_messagebox(json_resp.message, false);
                    }
                });
            }
        } else {
            display_messagebox('All values need to be specified');
        }

    });
    $('#save_bot').on('click', function(event){
        data = {
            'bot_class': $('#bot_class').val(),
            'bot_name': $('#bot_name').val(),
            'user_name':  $('#user_name').val(),
            'access_token_key': $('#access_token_key').val(),
            'access_token_secret': $('#access_token_secret').val(),
            'consumer_key': $('#consumer_key').val(),
            'consumer_secret': $('#consumer_secret').val(),
        };
        console.log(data);
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/create_bot/",
            data: JSON.stringify(data),
            dataType: 'json',
            success: function(_, _, result) {
                json_resp = result.responseJSON;
                display_messagebox(json_resp.message, true);
                $('#botform').collapse('hide');
                location.reload();
                $('#bot_name').val('');
                $('#user_name').val('');
                $('#passwd').val('');
                $('#access_token_key').val('');
                $('#access_token_secret').val('');
                $('#consumer_key').val('');
                $('#consumer_secret').val('');
            },
            error: function(result) {
                json_resp = result.responseJSON;
                display_messagebox(json_resp.message, false);
            }
        });
    });

    $('#export').on('click', function(event){
        // trigger an export of the database
        $.ajax({
            type: "POST",
            url: "/export_data/",
            datatype: 'json',
            success: function(_, _, result) {
                json_resp = result.responseJSON;
                display_messagebox(json_resp.message, true);
            },
            error: function(result){
                json_resp = result.responseJSON;
                display_messagebox(json_resp.message, true);
            }

        });
    });

});
