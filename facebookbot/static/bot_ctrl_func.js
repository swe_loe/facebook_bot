$(document).ready(function() {

    active = current_bot.active;
    bot_id = current_bot.bot_id;
    user_name = current_bot.user_name;
    console.log(user_name);

    message_box = $('.hidden_messagebox');
    content_top = $('#content_top');

    function display_messagebox(message, successful) {
        // display stylish messagebox here
        $('.message').text(message);
        if (successful == true) {
            content_top.append(message_box.clone().removeClass('hidden hidden_messagebox').addClass('alert-success displayed_messagebox'));
        } else {
            content_top.append(message_box.clone().removeClass('hidden hidden_messagebox').addClass('alert-danger displayed_messagebox'));
        }
        $('.displayed_messagebox').fadeOut(6000, function() {
            content_top.empty();
        });

    }
    start_button = $('#start_bot');
    $('#start_bot').on('click', function(event) {
        data = {
            'bot_id': bot_id
        };
        // if not actvie set to active
        if (!start_button.hasClass('active')) {
            event.preventDefault();
            $.ajax({
                type: "POST",
                contentType: "application/json",
                url: "/start_bot/",
                data: JSON.stringify(data),
                dataType: 'json',
                success: function(_, _, result) {
                    json_resp = result.responseJSON;
                    display_messagebox(json_resp.message, true);
                    active = true;
                    start_button.text('Stop Bot');
                    start_button.removeClass('btn-primary').addClass('btn-danger');
                },
                error: function(result) {
                    active = false;
                    json_resp = result.responseJSON;
                    console.log(json_resp.message);
                    display_messagebox(json_resp.message, false);
                }
            });
        } else {
            // if actvie set inactive
            event.preventDefault();
            $.ajax({
                type: "POST",
                contentType: "application/json",
                url: "/stop_bot/",
                data: JSON.stringify(data),
                dataType: 'json',
                success: function(_, _, result) {
                    json_resp = result.responseJSON;
                    display_messagebox(json_resp.message, true);
                    active = false;
                    start_button.text('Start Bot');
                    start_button.removeClass('btn-danger').addClass('btn-primary');
                },
                error: function(result) {
                    json_resp = result.responseJSON;
                    active = true;
                }
            });
        }
    });
    if (active == true) {
        start_button.text('Stop Bot');
        start_button.removeClass('btn-primary').addClass('btn-danger');
        start_button.addClass('active');
    } else {
        start_button.text('Start Bot');
        start_button.removeClass('active');
        start_button.removeClass('btn-danger').addClass('btn-primary');
        start_button.removeClass('aria-pressed');
    }

    var maxLength = 280;
    var length = $('textarea').val().length;

    if(length > 0){
        length = maxLength - length;
        $('#char_count').text(length);
    } else if(length == 0){
        $('#char_count').text('280');
    }
    $("textarea").keyup(function(event){
        length = $(this).val().length;
        length = maxLength - length;
        $('#char_count').text(length);
    });

    $('#send_tweet').on('click', function(event) {
        tweet = $('textarea').val();
        console.log(tweet.length);
        if(tweet.length == 0){
            display_messagebox("You can't send an empty tweet");
        } else{
            data = {
                'tweet': tweet,
                'bot_id': bot_id
            };
            console.log(data);
            $.ajax({
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify(data),
                url: "/send_tweet/",
                datatype: 'json',
                success: function(_, _, result) {
                    json_resp = result.responseJSON;
                    display_messagebox(json_resp.message, true);
                    $('textarea').val('');
                    length = $('textarea').val().length;
                    length = maxLength - length;
                    $('#char_count').text(length);
                },
                error: function(result) {
                    json_resp = result.responseJSON;
                    display_messagebox(json_resp.message, false);
                }
            });
        }
    });

    function get_timeline(user_name) {
        limit = 10;
        dnt = true;
        width = 200;
        user_url = 'url=https://twitter.com/' + user_name + '?limit=' + limit + '?maxwidth=' + width + '?dnt=' + dnt;
        data = {
            twitter_endpoint: "https://publish.twitter.com/oembed?" + user_url,
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            url: '/timeline_proxy/',
            data: JSON.stringify(data),
            success: function(_, _, result) {
                console.log('success');
                embed_info = result.responseJSON;
                $('#embedded_timeline').append(embed_info.html);
                tl_link = $('#embedded_timeline').find('.twitter-timeline');
                tl_link.attr('data-show-replies', 'true');
            },
            error: function(result) {
                console.log(result);
            }
        });
    }
    get_timeline(user_name);
});
