 $(document).ready(function() {
     // this script is for displaying/adding/removing rules
     // on the bot settings page

     message_box = $('.hidden_messagebox');
     content_top = $('#content_top');

     function display_messagebox(message, successful) {
         // display stylish messagebox here
         $('.message').text(message);
         if (successful == true) {
             content_top.append(message_box.clone().removeClass('hidden hidden_messagebox').addClass('alert-success displayed_messagebox'));
         } else {
             content_top.append(message_box.clone().removeClass('hidden hidden_messagebox').addClass('alert-danger displayed_messagebox'));
         }
         $('.displayed_messagebox').fadeOut(6000, function() {
             content_top.empty();
         });

     }

     function add_rule(rule_id, rule) {
         // adds a rule to the gui
         rule_template = $(".additional_rule");
         table_body = $("#table_body");
         new_rule = rule_template.clone().removeClass('hidden additional_rule');
         inp = new_rule.find('input').attr('value', rule).attr('name', rule_id);
         table_body.append(new_rule);
         return inp;
     }

     // initialize already existing rules
     if (rules) {
         $.each(rules, function(rule_idx, rule) {
             inp = add_rule(rule_idx, rule);
             inp.attr('readonly', true);
         });
     }
     $("#add_row").on("click", function() {
         add_rule();
     });

     $(window).resize(function() {
         if ($('html').width() <= 768) {
             $('.rule_input').removeClass('col-xs-10').addClass('col-xs-9');
         }
         if ($('html').width() >= 768) {
             $('.rule_input').removeClass('col-xs-9').addClass('col-xs-10');
         }
     });


     // send post request to remove rule from db
     $("#table_body").on("click", ".ibtnDel", function(event) {
         rule = $(this).closest('.bot_rule');
         rule_text = rule.find('input').val();
         rule_id = rule.find('input').attr('name');
         table_body = $('#table_body');
         data = {
             'bot_id': bot_id,
             'rule_id': rule_id
         };
         // catch unsaved empty rule deletion
         if (rule_id == '-1') {
             rule.remove();
             return;
         }

         if (!rule_id && rule_text) {
             message = "missing rule id";
             display_messagebox(message, false);
         } else {
             rule.remove();
             event.preventDefault();
             $.ajax({
                 type: "POST",
                 contentType: "application/json",
                 url: "/delete_rule/",
                 data: JSON.stringify(data),
                 dataType: 'json',
                 success: function(_, _, result) {
                     json_resp = result.responseJSON;
                     display_messagebox(json_resp.message, true);
                 },
                 error: function(result) {
                     json_resp = result.responseJSON;
                     display_messagebox(json_resp.message, false);
                 }
             });
         }
     });

     // send post request to save rule to db
     $("#table_body").on("click", ".ibtnSave", function(event) {
         rule = $(this).closest('.bot_rule');
         rule_text = rule.find('input').val();
         data = {
             'rule': rule_text,
             'bot_id': bot_id
         };
         $.ajax({
             type: "POST",
             contentType: "application/json;charset=UTF-8",
             url: "/save_rule/",
             data: JSON.stringify(data),
             dataType: 'json',
             success: function(_, _, result) {
                 json_resp = result.responseJSON;
                 rule.find('input').attr('name', json_resp.rule_id).attr('readonly', true);
                 display_messagebox(json_resp.message, true);
             },
             error: function(result) {
                 json_resp = result.responseJSON;
                 display_messagebox(json_resp.message, false);
             }
         });
     });
 });
