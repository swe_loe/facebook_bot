$(window).on('load', function() {
    /// this script handles how the chart for each bot is displayed
    'use strict';

    window.chartColors = {
        red: 'rgb(255, 99, 132)',
        orange: 'rgb(255, 159, 64)',
        yellow: 'rgb(255, 205, 86)',
        green: 'rgb(75, 192, 192)',
        blue: 'rgb(54, 162, 235)',
        purple: 'rgb(153, 102, 255)',
        grey: 'rgb(201, 203, 207)'
    };

    //############################################################
    // set the upper bounds of the chart
    var max_follower = Math.max.apply(null, current_bot.follower_count);
    var max_posted = Math.max.apply(null, current_bot.tweet_count);
    var max_received = Math.max.apply(null, current_bot.received_count);
    var max_retweet = Math.max.apply(null, current_bot.retweet_count);
    var scale_max = Math.max.apply(null, [max_follower, max_posted, max_received, max_retweet]);
    var length = current_bot.follower_count.length;
    var scale_x = ['now'];
    console.log(current_bot.follower_count);
    console.log(current_bot.tweet_count);
    console.log(current_bot.received_count);
    console.log(current_bot.retweet_count);
    for (var x = 1; x < length; x++) {
        scale_x.push("-" + x.toString());
    }
    // configuration for the chart
    // for details see https://www.chartjs.org/
    var config = {
        type: 'line',
        data: {
            labels: scale_x.reverse(),
            datasets: [{
                    label: 'Tweeted',
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    data: [],
                    fill: false,
                }, {
                    label: 'Received Tweets',
                    fill: false,
                    backgroundColor: window.chartColors.blue,
                    borderColor: window.chartColors.blue,
                    data: [],
                },
                {
                    label: 'Follower',
                    backgroundColor: window.chartColors.green,
                    borderColor: window.chartColors.green,
                    data: [],
                    fill: false,
                },
                {
                    label: 'Retweeted',
                    backgroundColor: window.chartColors.yellow,
                    borderColor: window.chartColors.yellow,
                    data: [],
                    fill: false,
                }
            ]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Bot Statistic'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Weeks'
                    }
                }],
                yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Amount'
                        },
                        ticks: {
                            min: 0,
                            max: scale_max
                        }
                    },
                    {
                        display: true,
                        position: 'right',
                        ticks: {
                            min: 0,
                            max: scale_max
                        }
                    }
                ]
            },
        }
    };

    var ctx = $('#mychart')[0].getContext('2d');
    window.myLine = new Chart(ctx, config);

    // fill the chart with data
    var colorNames = Object.keys(window.chartColors);
    ['Follower', "Tweeted", "Received", "Retweeted"].forEach(function(dat) {
        config.data.datasets.forEach(function(dataset) {
            if (dataset.label == "Follower") {
                current_bot.follower_count.forEach(function(val) {
                    dataset.data.push(val);
                });
            } else if (dataset.label == "Tweeted") {
                current_bot.tweet_count.forEach(function(val) {
                    dataset.data.push(val);
                });
            } else if (dataset.label == "Received Tweets") {
                current_bot.received_count.forEach(function(val) {
                    dataset.data.push(val);
                });

            } else if (dataset.label == "Retweeted"){
                current_bot.retweet_count.forEach(function(val){
                    dataset.data.push(val);
                });
            }
        });
    });
    window.myLine.update();
});
