$(document).ready(function() {
    // this script is for sliding in and out the menu sidebar

    $('#sidebarCollapse').on('click', function() {
        // collapse or expand sidebar on button click
        sidebar = $('#sidebar');
        content_width = $('#content').width();
        if (sidebar.hasClass('sidebar_visible')){
            sidebar.width('250px');
            $('#content').css('margin-left', '250px');
            sidebar.removeClass('sidebar_visible');
        } else {
            sidebar.width('0px');
            $('#content').css('margin-left', '0px');
            sidebar.addClass('sidebar_visible');
        }
    });



});
