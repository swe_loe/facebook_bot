#!/usr/bin/env python
from cheroot.wsgi import Server as WSGIServer
from facebookbot import create_app
from facebookbot.config import TestConfig, ProdConfig


def main():
    app = create_app(ProdConfig)
    server = WSGIServer(
        ('0.0.0.0', 5000),
        app,
        server_name="Twitter Bot Control Server",
        shutdown_timeout=1)
    try:
        server.start()
    except KeyboardInterrupt:
        server.stop()


if __name__ == '__main__':
    main()
