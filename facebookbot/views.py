#!/usr/bin/env python
from datetime import datetime
from flask import (render_template, request, redirect, url_for, Blueprint,
                   jsonify)
from pyparsing import ParseException
from requests import get
from pprint import pprint
from .models import TwitterBotModel, create_bot_hash, BotType
from .utils.rule_parser import SimpleParser
from .utils.error_handler import InvalidUsage
from .utils.db_helpers import (
    add_bot, save_rule_to_db, delete_rule_from_db, get_records_stats,
    save_defaultsettings, update_botsettings, get_defaultsettings, get_tokens,
    get_statistics, export_bots, export_users, export_tweets, export_rules,
    get_rules_with_id)
from .bot.twitter_bot import make_bot, get_user

bot = Blueprint('bot', __name__)

# dictionary where all active bots are stored. The dictionary keys are
# the ids of the bots
global active_bots
active_bots = {}


@bot.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    """
    generic error handler is called whenever InvalidUsage
    is instantiated.
    returns an error message
    """
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


# home = bot.route('/', home) <- this is what the decorator means
@bot.route('/')
def home():
    """
    This function is called when localhost:5000 is opened in the browser
    """
    all_bots = TwitterBotModel.select().where(TwitterBotModel.visible == 1)
    bots_w_status = []
    for bot in all_bots:
        stats = get_statistics(bot.id)
        bot_row = {
            'bot_name': bot.bot_name,
            'follower': stats['followers'],
            'following': stats['follows'],
            'bot_hash': bot.bot_hash
        }
        if bot.id in active_bots.keys():
            bot_row['status'] = 'active'
        else:
            bot_row['status'] = 'inactive'

        bots_w_status.append(bot_row)

    bot_classes = [x.bot_class for x in BotType.select()]

    return render_template(
        'home/home.html',
        all_bots=all_bots,
        bot_classes=bot_classes,
        bots_w_status=bots_w_status)


@bot.route('/save_rule/', methods=['POST'])
def save_rule():
    """
    this function is called by a function in static/dyntable.js and
    saves a rule which is passed as json
    """
    simple_parser = SimpleParser()
    data = request.get_json()
    rule = data['rule']
    try:
        rule_dict = simple_parser.parse(rule)
        rule_dict['rule'] = rule
        rule_id = save_rule_to_db(data['bot_id'], rule_dict)
    except ParseException:
        # on parse error return json with error message
        raise InvalidUsage('rule is in wrong format', status_code=400)
    # on parse success return success message
    return jsonify({
        'message': 'rule successfully added',
        'rule_id': rule_id  # needed to add rule_id as name attribute of rule
    }), 200


@bot.route('/delete_rule/', methods=['POST'])
def delete_rule():
    """
    this function is called by a function in static/dyntable.js and
    deletes a rule which is passed as json"""
    data = request.get_json()
    try:
        delete_rule_from_db(data['bot_id'], data['rule_id'])
        return jsonify({'message': 'rule successfully deleted'}), 200
    except Exception as e:
        return jsonify({'message': 'rule could not be deleted'}), 400


@bot.route('/start_bot/', methods=['POST'])
def start_bot():
    """
    This function is called when the user presses the start button
    on the bot page. It is called by a function in static/bot_ctrl_func.js
    """
    data = request.get_json()
    bot_id = data['bot_id']
    bot = TwitterBotModel.get_by_id(bot_id)
    query = BotType.get(BotType.id == bot.type_ref)

    bot_ref = [query.import_name, query.bot_class]
    try:
        active_bots[bot_id] = make_bot(get_tokens(bot_id), bot_ref, bot_id)
        active_bots[bot_id].start()
    except Exception as e:
        return jsonify({'message': e.args}), 200
    return jsonify({'message': 'bot started'}), 200


@bot.route('/stop_bot/', methods=['POST'])
def stop_bot():
    """
    This function is called when the user presses the stop button
    on the bot page. It is called by a function in static/bot_ctrl_func.js
    """
    data = request.get_json()
    bot_id = data['bot_id']
    if bot_id in active_bots:
        abot = active_bots[bot_id]
        abot.stop()
        active_bots.pop(bot_id, None)
    return jsonify({'message': 'bot stopped'}), 200


@bot.route('/bot/<bot_hash>/', methods=['GET', 'POST'])
def show_bot(bot_hash):
    """
    this function is called when the user select a bot in the sidebar and
    shows the information of a specific bot
    """
    bot_obj = TwitterBotModel.get(TwitterBotModel.bot_hash == bot_hash)
    all_bots = (TwitterBotModel.select().where(TwitterBotModel.visible == 1))
    # ^ dictionary
    bot_classes = [x.bot_class for x in BotType.select()]
    active = False
    if bot_obj.id in active_bots:
        active = True

    follower_count = get_records_stats(bot_obj.id, "follower", 8)
    retweet_count = get_records_stats(bot_obj.id, "retweeted", 8)
    received_count = get_records_stats(bot_obj.id, "mentions", 8)
    tweet_count = get_records_stats(bot_obj.id, "tweets", 8)

    srt_received_count = [
        value for (key, value) in sorted(received_count.items(), reverse=True)
    ]
    srt_follower_count = [
        value for (key, value) in sorted(follower_count.items(), reverse=True)
    ]
    srt_retweet_count = [
        value for (key, value) in sorted(retweet_count.items(), reverse=True)
    ]
    srt_tweet_count = [
        value for (key, value) in sorted(tweet_count.items(), reverse=True)
    ]

    current_bot = {
        'bot_id': bot_obj.id,
        'bot_name': bot_obj.bot_name,
        'active': active,
        'bot_hash': bot_obj.bot_hash,
        'user_name': bot_obj.user_name,
        'follower_count': srt_follower_count,
        'retweet_count': srt_retweet_count,
        'received_count': srt_received_count,
        'tweet_count': srt_tweet_count
    }

    # pprint(current_bot)
    return render_template(
        'twitterbot/twitterbot.html',
        all_bots=all_bots,
        current_bot=current_bot,
        bot_classes=bot_classes)


@bot.route('/bot/<bot_hash>/settings/', methods=['GET'])
def show_bot_settings(bot_hash):
    """
    This function is called when the user presses the settings button on
    a bot page and it shows the settings page for one specific bot
    """
    bot_obj = TwitterBotModel.get(TwitterBotModel.bot_hash == bot_hash)
    all_bots = (TwitterBotModel.select().where(TwitterBotModel.visible == 1))
    bot_classes = [x.bot_class for x in BotType.select()]
    active = False
    rules = get_rules_with_id(bot_obj.id)
    ruledict = {}
    for rule_id, rule in rules.items():
        ruledict[rule_id] = 'if tweet contains "{}" then "{}"'.format(
            rule[0], rule[1])

    if bot_obj.id in active_bots:
        active = True
    current_bot = {
        'id': bot_obj.id,
        'bot_name': bot_obj.bot_name,
        'active': active,
        'bot_hash': bot_hash,
        'min_resp_t': bot_obj.min_resp_t,
        'max_resp_t': bot_obj.max_resp_t,
        'routine_t': bot_obj.routine_t,
        'retweeted_sleep': bot_obj.retweeted_sleep,
        'follower_sleep': bot_obj.follower_sleep,
        'd_message_sleep': bot_obj.d_message_sleep,
    }
    default_settings = get_defaultsettings(1)
    return render_template(
        'settings/settings.html',
        current_bot=current_bot,
        all_bots=all_bots,
        rules=ruledict,
        default_settings=default_settings,
        bot_classes=bot_classes)


@bot.route('/settings/')
def show_common_settings():
    """
    this function is called when the user clicks the common settings
    button on any page and it shows the default settings which are used
    on bot creation
    """
    all_bots = (TwitterBotModel.select().where(TwitterBotModel.visible == 1))
    bot_classes = [x.bot_class for x in BotType.select()]

    default_settings = get_defaultsettings(1)
    return render_template(
        'settings/settings.html',
        all_bots=all_bots,
        default_settings=default_settings,
        bot_classes=bot_classes)


@bot.route('/delete_bot/', methods=['POST'])
def delete_bot():
    """
    this function is called when the user clicks the remove bot
    button on the bot settings page. It does not delete the
    but removes it from visible space.
    Due to the fact that the bot is not deleted from db its data
    will be in future exports
    """
    q = (TwitterBotModel.update({
        TwitterBotModel.visible: 0
    }).where(TwitterBotModel.id == request.form['bot_id']))
    q.execute()
    return redirect(url_for('bot.home'), code=302)


@bot.route('/create_bot/', methods=['POST'])
def create_bot():
    """
    this function is called when the user clicks the save bot button and
    creates a bot. It is called by a funcion in static/utils.js
    """
    try:
        data = request.get_json()
        query = BotType.get(BotType.bot_class == data['bot_class'])

        bot_ref = [query.import_name, query.bot_class]
        data['type_ref'] = query.id
        data['bot_hash'] = create_bot_hash(data['bot_name'])

        tokens = dict(filter(lambda i:i[0] in ['access_token_key',
                'access_token_secret','consumer_key','consumer_secret'], data.items()))

        user_dict = get_user(tokens, data['user_name'])
        bot_id = add_bot(data, user_dict)
        #to save the statistics
        make_bot(tokens, bot_ref, bot_id)
    except Exception as e:
        return jsonify({'message': e.args}), 400
    return jsonify({'message': 'Bot successfully created'}), 200


@bot.route('/ch_bot_settings/', methods=['POST'])
def change_bot_settings():
    """
    this function is called when the user clicks the save settings button
    on any settings page. If a bot_id is passed in the request
    this function changes the settings of the specified bot. If no
    bot_id is passed this function changes the default settings
    """
    data = request.get_json()
    min_resp_t = data['min_resp_t']
    max_resp_t = data['max_resp_t']
    routine_t = data['routine_t']
    retweeted_sleep = data['retweeted_sleep']
    follower_sleep = data['follower_sleep']
    d_message_sleep = data['d_message_sleep']
    if 'bot_id' in data.keys():
        bot_id = data['bot_id']
        try:
            update_botsettings(data)
            if bot_id in active_bots:
                active_bot = active_bots[bot_id]
                active_bot.set_response_time(min_resp_t, max_resp_t, routine_t)
                active_bot.set_listener_time(
                    [retweeted_sleep, follower_sleep, d_message_sleep])
            return jsonify({'message': 'settings saved'}), 200
        except Exception as e:
            return jsonify({'message': e.args}), 400
    else:
        save_defaultsettings(data)
        return jsonify({'message': 'default settings saved'}), 200


@bot.route('/timeline_proxy/', methods=['POST'])
def timeline_proxy():
    """
    this function is a helper function to fetch the
    timeline of the specified twitter user. It is called by
    a function in static/bot_ctrl_func.js. This is needed
    because we can't make cross-origin-XHRs
    """
    data = request.get_json()
    tw_endpoint = data['twitter_endpoint']
    resp = get(tw_endpoint)
    return jsonify(resp.json())


@bot.route('/export_data/', methods=['POST'])
def export_data():
    """
    this function is used to export the data from the database
    it is called by a function in static/utils.js when the
    user clicks the 'Export' button in the gui
    """
    try:
        date = datetime.now().strftime('%Y-%m-%d_%H-%M-%S-%f')
        export_bots(date)
        export_users(date)
        export_tweets(date)
        export_rules(date)
        return jsonify({'message': 'export successful'}), 200
    except Exception as e:
        return jsonify({'message': 'export failed'}), 400


@bot.route('/send_tweet/', methods=['POST'])
def send_tweet():
    """
    This function is used to post a tweet on twitter.
    It is only possible to post a tweet if the bot is active.
    This function is triggerd by a function in static/bot_ctrl_func.js when
    the user clicks the 'send tweet' button in the gui
    """
    try:
        data = request.get_json()
        bot_id = data['bot_id']
        if bot_id in active_bots.keys():
            active_bots[bot_id].tweet(data['tweet'])
            return jsonify({
                'message': 'tweet sent',
            }), 200
        else:
            return jsonify({
                'message': 'Bot needs to be active to send a tweet'
            }), 400
    except Exception as e:
        return jsonify({'message': e.args}), 200
