from setuptools import find_packages, setup

setup(
    name='facebookbot',
    version='0.0.1',
    url='git@gitlab.com:swe_loe/facebook_bot.git',
    maintainer='swe_loe',
    description='simple flask app for administration of social bots',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask', 'CherryPy', 'peewee', 'requests', 'tweepy', 'pymongo',
        'pyparsing'
    ],
    extras_require={
        'test': ['pytest']
    })
